<table width="100%" style="padding-top:20px;">
  <tbody>
    <tr>
      <td width="55%">
        <img src="<?php echo base_url('assets/img/asiarentacar.jpg'); ?>" alt="Asia Rent A Car" width="170" height="107" style="margin-left:-10px;" />
        <div style="font-size:16px; font-wieght:bold;" class="company-header">บริษัท เอเซีย เร้นท์ อะ คาร์ จำกัด</div>
        <div style="font-size:16px; font-wieght:bold;" class="company-header">Asia Rent A Car Company Limited (Owner)</div>
        <div style="font-size:5px;">&nbsp;</div>
        <div>35/32-33 ถนนแจ้งวัฒนะ แขวงทุ่งสองห้อง เขตหลักสี่ กรุงเทพฯ 10210</div>
        <div>35/32-33 Changwattana Road, Tungsonghong, Laksi, Bangkok 10210 Thailand</div>
        <div>Tel. +66(0) 2573 1132-3 Fax. +66(0) 2573 1144 Moblie phone +66 81 866 2555</div>
        <div>เลขประจำตัวผู้เสียภาษี Tax ID No. 0105550018941</div>
      </td>
      <td width="45%" style="padding-top:35px; text-align: right;">
        <p style="font-size:20px; font-wieght:bold;color:#000;">สัญญาเช่ารถยนต์</p>
        <p style="font-size:20px; font-wieght:bold;color:#000;">Vehicle Rental Agreement</p>
        <table class="outer-border-black" width="100%">
          <tr>
            <td style="border-bottom: 0.5px solid #000;" colspan="2">
              <table>
                <tr>
                  <td class="label-white-bg" width="50%">สัญญาเช่าเลขที่:<br/>
                  Rental Agreement No.</td>
                  <td class="label-white-bg data-value" width="50%" valign="middle"><?php echo $rent->rent_code; ?></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="border-bottom: 0.5px solid #000;" colspan="2">
              <table>
                <tr>
                  <td class="label-white-bg" width="50%">ใบยืนยันการจองเลขที่:<br/>
                  Reservation No.</td>
                  <td class="label-white-bg" width="50%"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="border-right: 0.5px solid #000;" width="50%">
              <table>
                <tr>
                  <td class="label-white-bg" width="50%">รหัสสินค้า:<br/>
                  ECD No.</td>
                  <td class="label-white-bg"  width="50%"></td>
                </tr>
              </table>
            </td>
            <td width="50%">
              <table>
                <tr>
                  <td class="label-white-bg">วันที่ทำสัญญา:<br/>
                  Date</td>
                  <td class="label-white-bg data-value" valign="middle"><?php echo $this->Datetime_service->display_date($rent->rent_date); ?></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
