<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="car rental system">
    <meta name="author" content="GongIdeas">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png') ?>">
    <title><?php echo $title; ?> - Asia Rent A Car</title>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?php echo base_url('assets/css/elegant-icons-style.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Prompt:400,700" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/bootstrap-datepicker-thai/css/datepicker.css'); ?>" rel="stylesheet" />
    <?php $this->Masterpage_service->print_css(); ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php $this->load->view('masterpage/navigate'); ?>
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <?php echo $content; ?>
          </div>
        </div>
      </section>
    </section>
    <!-- javascripts -->
    <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <!-- nice scroll -->
    <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>" type="text/javascript"></script><!--custome script for all page-->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url('assets/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
    <?php $this->Masterpage_service->print_js(); ?>
  </body>
</html>
