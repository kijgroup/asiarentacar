<!-- container section start -->
<section id="container" class="">
  <!--header start-->
  <header class="header dark-bg">
    <div class="toggle-nav">
      <div class="icon-reorder tooltips" data-original-title="Main Menu" data-placement="bottom"><i class="fa fa-bars"></i></div>
    </div>
    <!--logo start-->
    <?php echo anchor('rent', 'Asia Rent a Car', array('class'=>'logo')); ?>
    <!--logo end-->
    <div class="top-nav notification-row">
      <!-- notificatoin dropdown start-->
      <ul class="nav pull-right top-menu">
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <?php echo $this->session->userdata('admin_name'); ?>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu extended logout">
            <div class="log-arrow-up"></div>
            <li class="eborder-top">
              <?php echo anchor('admin/detail/'.$this->session->userdata('admin_id'), '<i class="icon_profile"></i> ข้อมูลส่วนตัว'); ?>
            </li>
            <li>
              <?php echo anchor('admin/changepassword/'.$this->session->userdata('admin_id'),'<i class="fa fa-lock"></i> เปลี่ยนรหัสผ่าน'); ?>
            </li>
            <li class="divider"></li>
            <li>
                <?php echo anchor('login/logout', '<i class="icon_key_alt"></i> ออกจากระบบ'); ?>
            </li>
          </ul>
        </li>
          <!-- user login dropdown end -->
      </ul>
      <!-- notificatoin dropdown end-->
    </div>
  </header>
<!--header end-->
<!--sidebar start-->
  <aside>
    <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
      <ul class="sidebar-menu">
        <li <?php echo ($nav == 'rent')?'class="active"':''; ?>>
            <?php echo anchor('rent','<i class="fa fa-file-text"></i> สัญญาเช่ารถยนต์'); ?>
        </li>
        <li <?php echo ($nav == 'customer')?'class="active"':''; ?>>
            <?php echo anchor('customer','<i class="fa fa-user"></i> ข้อมูลลูกค้า'); ?>
        </li>
        <li <?php echo ($nav == 'car')?'class="active"':''; ?>>
            <?php echo anchor('car','<i class="fa fa-car"></i> ข้อมูลรถ'); ?>
        </li>
        <li <?php echo ($nav == 'carmodel')?'class="active"':''; ?>>
            <?php echo anchor('carmodel','<i class="fa fa-tachometer"></i> ข้อมูลรุ่นรถ'); ?>
        </li>
        <li <?php echo ($nav == 'carbrand')?'class="active"':''; ?>>
            <?php echo anchor('carbrand','<i class="fa fa-building-o"></i> ข้อมูลยี่ห้อรถ'); ?>
        </li>
        <li <?php echo ($nav == 'admin')?'class="active"':''; ?>>
          <?php echo anchor('admin','<i class="fa fa-group"></i> ข้อมูลเจ้าหน้าที่'); ?>
        </li>
      </ul>
    <!-- sidebar menu end-->
    </div>
  </aside>
<!--sidebar end-->
</section>
