<h3 class="page-header"><i class="fa fa-group"></i>ข้อมูลลูกค้า</h3>
<div class="row" style="margin-bottom:20px; margin-left:16px;">
  <div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> การกระทำ <span class="caret"></span> </button>
    <ul class="dropdown-menu">
      <li><?php echo anchor('customer/form/'.$customer_id, 'แก้ไขข้อมูล');?></li>
      <li><?php echo anchor('customer/form/0', 'เพิ่มข้อมูล');?></li>
      <li class="divider"></li>
      <li><?php echo anchor('customer/delete/'.$customer_id,'ลบข้อมูล'); ?></li>
    </ul>
  </div>
  <?php echo anchor('customer/index','กลับไปหน้ารายการ', array('class'=>'btn btn-danger')); ?>
</div>

<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลลูกค้า
    </header>
    <div class="panel-body">
      <div class="form-horizontal">
        <div class="form-group">
          <label class="control-label col-lg-2">บริษัท : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->company; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">เลขประจำตัวผู้เสียภาษี : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->tax_id; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ชื่อ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->first_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">นามสกุล : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->last_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">เลขที่บัตรประชาชน : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->id_card; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">อายุ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->age; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">วันเกิด : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_date($customer->birthday); ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ออกโดย : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->issue_card; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ที่อยู่ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->address; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">โทรศัพท์ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->phone; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">มือถือ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->mobile; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">E-mail : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->email; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">เลขที่ใบขับขี่ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->driver_license_id; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ออกโดย : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->issue_driver; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">วันหมดอายุ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_date($customer->end_date_card); ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">หมวด : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $customer->category; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Created : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($customer->create_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($customer->create_by); ?></p>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Updated : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($customer->update_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($customer->update_by); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
