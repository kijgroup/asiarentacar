<h3 class="page-header"><i class="fa fa-user"></i> <?php echo $title; ?></h3>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลลูกค้า
    </header>
    <?php echo form_open('customer/form_post/'.$customer_id, array('class'=>'form-validate form-horizontal')); ?>
    <div class="panel-body">
      <div class="form-group ">
        <label class="control-label col-lg-3">บริษัท</label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="company" value="<?php echo($customer_id !=0)? $customer->company:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">เลขประจำตัวผู้เสียภาษี <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="tax_id" value="<?php echo($customer_id !=0)? $customer->tax_id:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">คำนำหน้าชื่อ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="pre_name" id="pre_name" value="<?php echo($customer_id !=0)? $customer->pre_name:''; ?>" placeholder="Mr., Mrs., Miss, นาย, นาง, นางสาว, ..." />
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">ชื่อ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="first_name" value="<?php echo($customer_id !=0)? $customer->first_name:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">นามสกุล <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="last_name" value="<?php echo($customer_id !=0)? $customer->last_name:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">เลขที่บัตรประชาชน <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="id_card" value="<?php echo($customer_id !=0)? $customer->id_card:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">อายุ (ปี) <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="age" value="<?php echo($customer_id !=0)? $customer->age:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">ออกโดย <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="issue_card" value="<?php echo($customer_id !=0)? $customer->issue_card:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">วันเกิด <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control inp_date" name="birthday" value="<?php echo($customer_id !=0)? $this->Datetime_service->display_date($customer->birthday):''; ?>" placeholder="31/12/2560" />
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">ที่อยู่ <span class="required">*</span></label>
        <div class="col-lg-6">
          <textarea class="form-control" name="address"><?php echo($customer_id !=0)? $customer->address:''; ?></textarea>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">โทรศัพท์ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="phone" value="<?php echo($customer_id !=0)? $customer->phone:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">มือถือ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="mobile" value="<?php echo($customer_id !=0)? $customer->mobile:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">E-mail <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="email" value="<?php echo($customer_id !=0)? $customer->email:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">เลขที่ใบขับขี่ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="driver_license_id" value="<?php echo($customer_id !=0)? $customer->driver_license_id:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">ออกโดย <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="issue_driver" value="<?php echo($customer_id !=0)? $customer->issue_driver:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">วันหมดอายุ <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="end_date_card" value="<?php echo($customer_id !=0)? $this->Datetime_service->display_date($customer->end_date_card):''; ?>" placeholder="31/12/2560"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">หมวด <span class="required">*</span></label>
        <div class="col-lg-6">
          <input type="text" class="form-control" name="category" value="<?php echo($customer_id !=0)? $customer->category:''; ?>"/>
        </div>
      </div>

      <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
          <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
          <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
        </div>
      </div>
    </div>
    <?php echo form_close(); ?>
  </section>
</div>
