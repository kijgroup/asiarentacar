<div style="padding:10px 0;">
  มีทั้งหมด <?php echo $total_transaction; ?> คน
</div>
<section class="panel">
  <table class="table table-hover">
    <thead>
      <tr>
        <th width="40">#</th>
        <th>บริษัท</th>
        <th>ชื่อ</th>
        <th>หมวด</th>
      </tr>
    </thead>
    <tbody>
    <?php
      foreach($customer_list->result() as $customer){
        $detail_url = 'customer/detail/'.$customer->customer_id;
      ?>
      <tr>
        <td><?php echo $customer->customer_id; ?></td>
        <td><?php echo anchor($detail_url, ($customer->company != '')?$customer->company:'-'); ?></td>
        <td><?php echo anchor($detail_url, $customer->pre_name.' '.$customer->first_name.' '.$customer->last_name); ?></td>
        <td><?php echo anchor($detail_url, ($customer->category != '')?$customer->category:'-'); ?></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
</section>
<?php
$total_page = ceil($total_transaction/$per_page);
$this->load->view('customer/list/pager', array(
  'total_transaction' => $total_transaction,
  'search_val' => $search_val,
  'per_page' => $per_page,
  'page' => $page,
  'total_page' => $total_page,
  'action' => site_url('customer/filter/'),
));
