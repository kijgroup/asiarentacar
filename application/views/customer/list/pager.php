<div>
  <form class="form-inline">
    <div class="pull-right">หน้า
    <div class="form-group">
      <select name="ddlPager" class="ddlPager form-control"
        data-search_val="<?php echo $search_val; ?>"
        data-per_page="<?php echo $per_page; ?>"
        data-action="<?php echo $action; ?>"
        >
        <?php
        for($i = 1; $i <= $total_page; $i++){
          $selected = ($i == $page)?'selected="selected"':'';
          echo '<option val="'.$i.'" '.$selected.'>'.$i.'</option>';
        }
        ?>
      </select>
    </div>
    / <span class="int"><?php echo $total_page; ?></span>
    </div>
  </form>
</div>
