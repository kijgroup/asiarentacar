<h3 class="page-header"><i class="fa fa-file-text"></i> สัญญาเช่ารถยนต์</h3>
<div id="search-customer-group" class="form-horizontal" data-searchurl="<?php echo site_url('search_customer'); ?>" <?php echo ($rent_id != 0)?'style="display:none;"':''; ?>>
  <?php $this->load->view('rent/search_customer/search_form'); ?>
</div>
<div class="col-lg-12" id="main-form" <?php echo ($rent_id == 0)?'style="display:none;"':''; ?>>
  <?php echo form_open('rent/form_post/'.$rent_id, array('class'=>'form-validate')); ?>
  <input type="hidden" name="customer_id" id="customer_id" value="<?php echo ($rent_id != 0)?$rent->customer_id:0; ?>" />
  <section class="panel">
    <?php
    $this->load->view('rent/form/panel1');
    $this->load->view('rent/form/panel2');
    $this->load->view('rent/form/panel3');
    $this->load->view('rent/form/panel4');
    $this->load->view('rent/form/panel5');
    $this->load->view('rent/form/panel6');
    $this->load->view('rent/form/panel7');
    $this->load->view('rent/form/panel8');
    $this->load->view('rent/form/panel9');
    ?>
    <div class="panel-footer text-right">
      <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
      <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
    </div>
  </section>
  <?php echo form_close(); ?>
</div>
