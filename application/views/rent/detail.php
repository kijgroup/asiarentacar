<h3 class="page-header"><i class="fa fa-file-text"></i> สัญญาเช่ารถยนต์</h3>
<div class="row" style="margin-bottom:20px; margin-left:16px;">
  <div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> การกระทำ <span class="caret"></span> </button>
    <ul class="dropdown-menu">
      <li><?php echo anchor('rent/form/'.$rent_id, 'แก้ไขข้อมูล');?></li>
      <li class="divider"></li>
      <li><?php echo anchor('rent/delete/'.$rent_id,'ลบข้อมูล'); ?></li>
    </ul>
  </div>
  <?php echo anchor('rent/index','กลับไปหน้ารายการ', array('class'=>'btn btn-danger')); ?>
  <?php echo anchor('rent/print_pdf/'.$rent_id, '<i class="fa fa-print"></i> download PDF', array('class'=>'btn btn-success', 'target'=> '_blank')); ?>
</div>
<style>
.panel .form-control-static{
  border-bottom:1px solid #ccc;
  min-height:27px;
}
.panel pre{
  min-height:54px;
}
</style>
<div class="col-lg-12">
  <section class="panel">
    <?php
    $this->load->view('rent/detail/panel1');
    $this->load->view('rent/detail/panel2');
    $this->load->view('rent/detail/panel3');
    $this->load->view('rent/detail/panel4');
    $this->load->view('rent/detail/panel5');
    $this->load->view('rent/detail/panel6');
    $this->load->view('rent/detail/panel7');
    $this->load->view('rent/detail/panel8');
    $this->load->view('rent/detail/panel9');
    ?>
  </section>
</div>
