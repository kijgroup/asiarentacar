<header class="panel-heading">
  2. ข้อมูลผู้เช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_company" class="control-label">ชื่อบริษัท</label>
        <input type="text" class="form-control" name="rent_company" id="rent_company" value="<?php echo($rent_id !=0)? $rent->rent_company:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_tax_id" class="control-label">เลขประจำตัวผู้เสียภาษี</label>
        <input type="text" class="form-control" name="rent_tax_id" id="rent_tax_id" value="<?php echo($rent_id !=0)? $rent->rent_tax_id:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="category" class="control-label">หมวด</label>
        <input type="text" class="form-control" name="category" id="category" value="<?php echo($rent_id !=0)? $customer->category:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-2">
      <div class="form-group">
        <label for="rent_pre_name" class="control-label">คำนำหน้าชื่อ</label>
        <input type="text" class="form-control pre_name" name="rent_pre_name" id="rent_pre_name" value="<?php echo($rent_id !=0)? $rent->rent_pre_name:''; ?>" placeholder="Mr., Mrs., Miss, นาย, นาง, นางสาว, ..." />
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label for="rent_first_name" class="control-label">ชื่อ <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_first_name" id="rent_first_name" value="<?php echo($rent_id !=0)? $rent->rent_first_name:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label for="rent_last_name" class="control-label">นามสกุล <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_last_name" id="rent_last_name" value="<?php echo($rent_id !=0)? $rent->rent_last_name:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_age" class="control-label">อายุ (ปี)</label>
        <input type="text" class="form-control" name="rent_age" id="rent_age" value="<?php echo($rent_id !=0)? $rent->rent_age:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_phone" class="control-label">โทรศัพท์</label>
        <input type="text" class="form-control" name="rent_phone" id="rent_phone" value="<?php echo($rent_id !=0)? $rent->rent_phone:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_mobile" class="control-label">มือถือ</label>
        <input type="text" class="form-control" name="rent_mobile" id="rent_mobile" value="<?php echo($rent_id !=0)? $rent->rent_mobile:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_email" class="control-label">E-Mail</label>
        <input type="email" class="form-control" name="rent_email" id="rent_email" value="<?php echo($rent_id !=0)? $rent->rent_email:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8">
      <div class="form-group ">
        <label for="rent_address" class="control-label">ที่อยู่ปัจจุบัน</label>
        <textarea class="form-control" id="rent_address" name="rent_address" /><?php echo($rent_id !=0)? $rent->rent_address:''; ?></textarea>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_driver_license_id" class="control-label">เลขที่ใบขับขี่ <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_driver_license_id" id="rent_driver_license_id"  value="<?php echo($rent_id !=0)? $rent->rent_driver_license_id:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_issue_driver" class="control-label">ออกโดย</label>
        <input type="text" class="form-control" name="rent_issue_driver" id="rent_issue_driver" value="<?php echo($rent_id !=0)? $rent->rent_issue_driver:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_birthday" class="control-label">วันเกิด</label>
        <input type="text" class="form-control inp_date" name="rent_birthday" id="rent_birthday" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_birthday):''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_id_card" class="control-label">เลขที่บัตรประชาชน</label>
        <input type="text" class="form-control" name="rent_id_card" id="rent_id_card" value="<?php echo($rent_id !=0)? $rent->rent_id_card:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_issue_card" class="control-label">ออกโดย</label>
        <input type="text" class="form-control" name="rent_issue_card" id="rent_issue_card" value="<?php echo($rent_id !=0)? $rent->rent_issue_card:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_end_date_card" class="control-label">วันหมดอายุ</label>
        <input type="text" class="form-control inp_date" name="rent_end_date_card" id="rent_end_date_card" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_end_date_card):''; ?>" placeholder="31/12/2560" />
      </div>
    </div>
  </div>
</div>
