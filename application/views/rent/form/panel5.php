<header class="panel-heading">
  5. ข้อมูลการรับรถ
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_start_date" class="control-label">วันที่ <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_start_date" id="rent_start_date" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_start_date):''; ?>" required="required" placeholder="31/12/2560"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_start_time" class="control-label">เวลา <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_start_time" id="rent_start_time" value="<?php echo($rent_id !=0)? $rent->rent_start_time:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_start_km" class="control-label">เลขกิโลเมตร</label>
        <input type="text" class="form-control" name="rent_start_km" id="rent_start_km" value="<?php echo($rent_id !=0)? $rent->rent_start_km:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="rent_start_location" class="control-label">สถานที่</label>
    <input type="text" class="form-control" name="rent_start_location" id="rent_start_location" value="<?php echo($rent_id !=0)? $rent->rent_start_location:''; ?>"/>
  </div>
</div>
