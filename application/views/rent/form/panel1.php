<header class="panel-heading">
  1. ข้อมูลใบเช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_code" class="control-label">เลขที่สัญญา <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_code" id="rent_code" value="<?php echo($rent_id !=0)? $rent->rent_code:''; ?>" required="required" />
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_date" class="control-label">วันที่ทำสัญญา <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control inp_date" name="rent_date" id="rent_date" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_date):''; ?>" required="required" placeholder="31/12/2560" />
      </div>
    </div>
  </div>
</div>
