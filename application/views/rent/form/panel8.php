<header class="panel-heading">
  8. เงื่อนไขการชำระเงิน
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ชำระเงินโดย <span class="required">*</span></label>
        <select class="form-control" name="rent_payment" id="rent_payment" value="<?php echo($rent_id !=0)? $rent->rent_payment:''; ?>" required="required"/>
          <option value="">โปรดเลือก</option>
          <?php
          $payment_type_list = array(
            'creditcard' => 'Credit Card',
            'cash' => 'เงินสด',
            'bill' => 'วางบิลบริษัท',
            'credit' => 'เครดิต',
            'other' => 'อื่น ๆ',
          );
          foreach($payment_type_list as $key => $value){
            $selected = ($rent_id != 0 && $rent->rent_payment == $key)?' selected="selected"':'';
            echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row payment" id="payment_type_creditcard">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_type_credit_card" class="control-label">ประเภทบัตร <span class="required" aria-required="true">*</span></label>
        <select class="form-control m-bot15" name="rent_type_credit_card" id="rent_type_credit_card" value="<?php echo($rent_id !=0)? $rent->rent_type_credit_card:''; ?>"/>
          <option>Master Card</option>
          <option>Visa</option>
        </select>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขที่บัตร</label>
        <input type="text" class="form-control" name="rent_credit_card_id" id="rent_credit_card_id" value="<?php echo($rent_id !=0)? $rent->rent_credit_card_id:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">วันหมดอายุ</label>
        <input type="text" class="form-control" name="rent_credit_card_end_date" id="rent_credit_card_end_date" value="<?php echo($rent_id !=0)? $rent->rent_credit_card_end_date:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row payment" id="payment_type_bill">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_company_bill" class="control-label">บริษัท</label>
        <input type="text" class="form-control" name="rent_company_bill" id="rent_company_bill" value="<?php echo($rent_id !=0)? $rent->rent_company_bill:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-12 col-md-8">
      <div class="form-group">
        <label for="rent_bill_address" class="control-label">ที่อยู่บริษัท</label>
        <textarea class="form-control" id="rent_bill_address" name="rent_bill_address" value="<?php echo($rent_id !=0)? $rent->rent_bill_address:''; ?>"/></textarea>
      </div>
    </div>
  </div>
  <div class="row payment" id="payment_type_credit">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_bill_credit_day" class="control-label">เครดิต (วัน)</label>
        <input type="text" class="form-control" name="rent_bill_credit_date" id="rent_bill_credit_date" value="<?php echo($rent_id !=0)? $rent->rent_bill_credit_date:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row payment" id="payment_type_other">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_payment_other" class="control-label">ชำระด้วยวิธีอื่น ๆ</label>
        <input type="text" class="form-control" name="rent_payment_other" id="rent_payment_other" value="<?php echo($rent_id !=0)? $rent->rent_payment_other:''; ?>"/>
      </div>
    </div>
  </div>
</div>
