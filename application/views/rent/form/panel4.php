<header class="panel-heading">
  4. ข้อมูลรถที่เช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="carbrand" class="control-label">ยี่ห้อรถ <span class="required">*</span></label>
        <select class="form-control" name="carbrand" id="carbrand" required="required">
          <option value="">โปรดระบุ</option>
          <?php
            $carbrand_list = $this->Carbrand_model->get_list();
            foreach($carbrand_list->result() as $brand){
              $selected = ($rent_id != 0 && !$carbrand && $carbrand->brand_id == $brand->brand_id)?' selected="selected"':'';
              echo '<option value="'.$brand->brand_id.'"'.$selected.'>'.$brand->brand_name.'</option>';
            }
          ?>
        </select>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="carmodel" class="control-label">รุ่นรถ <span class="required">*</span></label>
        <select class="form-control" name="carmodel" id="carmodel" required="required">
          <option value="" data-brand="">โปรดระบุ</option>
          <?php
            $carmodel_list = $this->Carmodel_model->get_list();
            foreach($carmodel_list->result() as $model){
              $selected = ($rent_id != 0 && !$carmodel && $carmodel->model_id == $model->model_id)?' selected="selected"':'';
              echo '<option value="'.$model->model_id.'" class="model-brand model-brand-'.$model->brand_id.'" data-brand="'.$model->brand_id.'"'.$selected.'>['.$model->car_type.'] '.$model->model_name.'</option>';
            }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="car" class="control-label">ทะเบียนรถ <span class="required">*</span></label>
        <select class="form-control" name="car" id="car" required="required">
          <option value="" data-brand="" data-model="" data-color="โปรดระบุทะเบียนรถ">โปรดระบุ</option>
          <?php
            $car_list = $this->Car_model->get_list();
            foreach($car_list->result() as $car){
              $selected = ($rent_id != 0 && $rent->car_id == $car->car_id)?' selected="selected"':'';
              $model = $this->Carmodel_model->get_data($car->model_id);
              echo '<option value="'.$car->car_id.'" class="car-model car-model-'.$car->model_id.' car-brand car-brand-'.$model->brand_id.'" data-brand="'.$model->brand_id.'" data-model="'.$car->model_id.'" data-color="'.$car->car_color.'"'.$selected.'>'.$car->car_registration.'</option>';
            }
          ?>
        </select>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">สีรถ</label>
        <p class="form-control" id="car_color" disabled>โปรดระบุทะเบียนรถ</p>
      </div>
    </div>
  </div>
</div>
