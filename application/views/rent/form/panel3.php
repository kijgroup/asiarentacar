<header class="panel-heading">
  3. ข้อมูลผู้ขับร่วม
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-2">
      <div class="form-group">
        <label for="rent_buddy_pre_name" class="control-label">คำนำหน้าชื่อ</label>
        <input type="text" class="form-control pre_name" name="rent_buddy_pre_name" id="rent_buddy_pre_name" value="<?php echo($rent_id !=0)? $rent->rent_buddy_pre_name:''; ?>" placeholder="Mr., Mrs., Miss, นาย, นาง, นางสาว, ..." />
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label for="rent_buddy_first_name" class="control-label">ชื่อ</label>
        <input type="text" class="form-control" name="rent_buddy_first_name" id="rent_buddy_first_name" value="<?php echo($rent_id !=0)? $rent->rent_buddy_first_name:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label for="rent_buddy_last_name" class="control-label">นามสกุล</label>
        <input type="text" class="form-control" name="rent_buddy_last_name" id="rent_buddy_last_name" value="<?php echo($rent_id !=0)? $rent->rent_buddy_last_name:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_age" class="control-label">อายุ (ปี)</label>
        <input type="text" class="form-control" name="rent_buddy_age" id="rent_buddy_age" value="<?php echo($rent_id !=0)? $rent->rent_buddy_age:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_phone" class="control-label">โทรศัพท์</label>
        <input type="text" class="form-control" name="rent_buddy_phone" id="rent_buddy_phone" value="<?php echo($rent_id !=0)? $rent->rent_buddy_phone:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_mobile" class="control-label">มือถือ</label>
        <input type="text" class="form-control" name="rent_buddy_mobile" id="rent_buddy_mobile" value="<?php echo($rent_id !=0)? $rent->rent_buddy_mobile:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8">
      <div class="form-group">
        <label for="rent_buddy_address" class="control-label">ที่อยู่</label>
        <textarea class="form-control" id="rent_buddy_address" name="rent_buddy_address"><?php echo($rent_id !=0)? $rent->rent_buddy_address:''; ?></textarea>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_driver_license" class="control-label">เลขที่ใบขับขี่</label>
        <input type="text" class="form-control" name="rent_buddy_driver_license" id="rent_buddy_driver_license" value="<?php echo($rent_id !=0)? $rent->rent_buddy_driver_license:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_issue_driver" class="control-label">ออกโดย</label>
        <input type="text" class="form-control" name="rent_buddy_issue_driver" id="rent_buddy_issue_driver" value="<?php echo($rent_id !=0)? $rent->rent_buddy_issue_driver:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_buddy_end_date" class="control-label">วันหมดอายุ</label>
        <input type="text" class="form-control inp_date" name="rent_buddy_end_date" id="rent_buddy_end_date" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_buddy_end_date):''; ?>" placeholder="31/12/2560"/>
      </div>
    </div>
  </div>
</div>
