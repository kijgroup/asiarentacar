<header class="panel-heading">
  6. ข้อมูลการคืนรถ
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_end_date" class="control-label">วันที่ <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_end_date" id="rent_end_date" value="<?php echo($rent_id !=0)? $this->Datetime_service->display_date($rent->rent_end_date):''; ?>" required="required" placeholder="31/12/2560"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_end_time" class="control-label">เวลา <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control" name="rent_end_time" id="rent_end_time" value="<?php echo($rent_id !=0)? $rent->rent_end_time:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_end_km" class="control-label">เลขกิโลเมตร</label>
        <input type="text" class="form-control" name="rent_end_km" id="rent_end_km" value="<?php echo($rent_id !=0)? $rent->rent_end_km:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="rent_end_location" class="control-label">สถานที่</label>
    <input type="text" class="form-control" name="rent_end_location" id="rent_end_location" value="<?php echo($rent_id !=0)? $rent->rent_end_location:''; ?>"/>
  </div>
</div>
