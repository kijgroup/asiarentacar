<header class="panel-heading">
  7. รายละเอียดค่าเช่ารถ
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_inclusive_rate" class="control-label">ค่ารถเช่ารายวัน วันละ (บาท) <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control inp_price" name="rent_inclusive_rate" id="rent_inclusive_rate" value="<?php echo($rent_id !=0)? $rent->rent_inclusive_rate:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_totalday" class="control-label">จำนวนวันที่เช่า (วัน) <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control inp_price" name="rent_totalday" id="rent_totalday" value="<?php echo($rent_id !=0)? $rent->rent_totalday:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_total" class="control-label">รวมเป็นเงิน (บาท) <span class="required" aria-required="true">*</span></label>
        <input type="text" class="form-control inp_price" name="rent_total" id="rent_total" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_total:''; ?>" readonly="readonly"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าประกันภัยรถยนต์</label>
        <input type="text" class="form-control inp_price" name="rent_total_ldw" id="rent_total_ldw" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_total_ldw:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าประกันภัยส่วนบุคคล</label>
        <input type="text" class="form-control inp_price" name="rent_total_pae" id="rent_total_pae" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_total_pae:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่ารับ/ส่งรถยนต์</label>
        <input type="text" class="form-control inp_price" name="rent_total_delivery_and_pickup" id="rent_total_delivery_and_pickup" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_total_delivery_and_pickup:''; ?>"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าใช้จ่ายอื่นๆ</label>
        <input type="text" class="form-control inp_price" name="rent_total_other" id="rent_total_other" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_total_other:''; ?>"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รวมราคา (บาท)</label>
        <input type="text" class="form-control inp_price" name="rent_sub_total" id="rent_sub_total" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_sub_total:''; ?>" readonly="readonly"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">VAT 7% (บาท)</label>
        <input type="text" class="form-control inp_price" name="rent_vat" id="rent_vat" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_vat:''; ?>" required="required"/>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รวมค่าเช่าทั้งหมด (บาท)</label>
        <input type="text" class="form-control inp_price" name="rent_grand_total" id="rent_grand_total" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_grand_total:''; ?>" readonly="readonly"/>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label">* หากน้ำมันไม่เต็มถังในวันที่คืนรถ ผู้เชาต้องรับผิดชอบชำระค่าน้ำมันลิตรละ (บาท)</label>
    <input type="text" class="form-control" name="rent_oil_price" id="rent_oil_price" placeholder="0.00" value="<?php echo($rent_id !=0)? $rent->rent_oil_price:''; ?>"/>
  </div>
</div>
