<header class="panel-heading">
  1. ข้อมูลใบเช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขที่สัญญา</label>
        <p class="form-control-static"><?php echo $rent->rent_code; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">วันที่ทำสัญญา</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_date); ?></p>
      </div>
    </div>
  </div>
</div>
