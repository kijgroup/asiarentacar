<header class="panel-heading">
  3. ข้อมูลผู้ขับร่วม
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-2">
      <div class="form-group">
        <label class="control-label">คำนำหน้าชื่อ</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_pre_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label class="control-label">ชื่อ</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_first_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label class="control-label">นามสกุล</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_last_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">อายุ (ปี)</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_age; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">โทรศัพท์</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_phone; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">มือถือ</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_mobile; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8">
      <div class="form-group">
        <label class="control-label">ที่อยู่</label>
        <pre><?php echo $rent->rent_buddy_address; ?></pre>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขที่ใบขับขี่</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_driver_license; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ออกโดย</label>
        <p class="form-control-static"><?php echo $rent->rent_buddy_issue_driver; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">วันหมดอายุ</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_buddy_end_date); ?></p>
      </div>
    </div>
  </div>
</div>
