<header class="panel-heading">
  7. รายละเอียดค่าเช่ารถ
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่ารถเช่ารายวัน วันละ (บาท) <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_inclusive_rate; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">จำนวนวันที่เช่า (วัน) <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_totalday; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รวมเป็นเงิน (บาท) <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_total; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าประกันภัยรถยนต์</label>
        <p class="form-control-static"><?php echo $rent->rent_total_ldw; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าประกันภัยส่วนบุคคล</label>
        <p class="form-control-static"><?php echo $rent->rent_total_pae; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่ารับ/ส่งรถยนต์</label>
        <p class="form-control-static"><?php echo $rent->rent_total_delivery_and_pickup; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ค่าใช้จ่ายอื่นๆ</label>
        <p class="form-control-static"><?php echo $rent->rent_total_other; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รวมราคา (บาท)</label>
        <p class="form-control-static"><?php echo $rent->rent_sub_total; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">VAT 7% (บาท)</label>
        <p class="form-control-static"><?php echo $rent->rent_vat; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รวมค่าเช่าทั้งหมด (บาท)</label>
        <p class="form-control-static"><?php echo $rent->rent_grand_total; ?></p>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label">* หากน้ำมันไม่เต็มถังในวันที่คืนรถ ผู้เชาต้องรับผิดชอบชำระค่าน้ำมันลิตรละ (บาท)</label>
    <p class="form-control-static"><?php echo $rent->rent_oil_price; ?></p>
  </div>
</div>
