<header class="panel-heading">
  8. เงื่อนไขการชำระเงิน
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ชำระเงินโดย</label>
        <p class="form-control-static"><?php echo $rent->rent_payment; ?></p>
      </div>
    </div>
  </div>
  <?php if($rent->rent_payment == 'creditcard'){ ?>
  <div class="row" id="payment_type_creditcard">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ประเภทบัตร</label>
        <p class="form-control-static"><?php echo $rent->rent_type_credit_card; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขที่บัตร</label>
        <p class="form-control-static"><?php echo $rent->rent_credit_card_id; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">วันหมดอายุ</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_credit_card_end_date); ?></p>
      </div>
    </div>
  </div>
  <?php
  }
  if($rent->rent_payment == 'bill'){
  ?>
  <div class="row" id="payment_type_bill">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">บริษัท</label>
        <p class="form-control-static"><?php echo $rent->rent_company_bill; ?></p>
      </div>
    </div>
    <div class="col-sm-12 col-md-8">
      <div class="form-group">
        <label class="control-label">ที่อยู่บริษัท</label>
        <pre><?php echo $rent->rent_bill_address; ?></pre>
      </div>
    </div>
  </div>
  <?php
  }
  if($rent->rent_payment == 'credit'){
  ?>
  <div class="row" id="payment_type_credit">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เครดิต (วัน)</label>
        <p class="form-control-static"><?php echo $rent->rent_bill_credit_date; ?></p>
      </div>
    </div>
  </div>
  <?php
  }
  if($rent->rent_payment == 'other'){
  ?>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ชำระด้วยวิธีอื่น ๆ</label>
        <p class="form-control-static"><?php echo $rent->rent_payment_other; ?></p>
      </div>
    </div>
  </div>
  <?php
  }
  ?>
</div>
