<header class="panel-heading">
  9. หมายเหตุ
</header>
<div class="panel-body">
  <div class="form-group">
    <label class="control-label">รายละเอียดเพิ่มเติม</label>
    <pre><?php echo $rent->rent_note; ?></pre>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">Created</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($rent->create_date); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">By</label>
        <p class="form-control-static"><?php echo $this->Admin_model->get_name($rent->create_by); ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">Updated</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($rent->update_date); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">By</label>
        <p class="form-control-static"><?php echo $this->Admin_model->get_name($rent->update_by); ?></p>
      </div>
    </div>
  </div>
</div>
