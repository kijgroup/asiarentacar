<header class="panel-heading">
  4. ข้อมูลรถที่เช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ยี่ห้อรถ</label>
        <p class="form-control-static"><?php echo $carbrand->brand_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รุ่นรถ</label>
        <p class="form-control-static"><?php echo $carmodel->model_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">รหัสรถ</label>
        <p class="form-control-static"><?php echo $carmodel->car_type; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ทะเบียนรถ</label>
        <p class="form-control-static"><?php echo $car->car_registration; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">สีรถ</label>
        <p class="form-control-static"><?php echo $car->car_color; ?></p>
      </div>
    </div>
  </div>
</div>
