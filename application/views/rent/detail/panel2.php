<header class="panel-heading">
  2. ข้อมูลผู้เช่า
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">ชื่อบริษัท</label>
        <p class="form-control-static"><?php echo $rent->rent_company; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขประจำตัวผู้เสียภาษี</label>
        <p class="form-control-static"><?php echo $rent->rent_tax_id; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-2">
      <div class="form-group">
        <label class="control-label">คำนำหน้าชื่อ</label>
        <p class="form-control-static"><?php echo $rent->rent_pre_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label class="control-label">ชื่อ <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_first_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-3">
      <div class="form-group">
        <label class="control-label">นามสกุล <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_last_name; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">อายุ (ปี)</label>
        <p class="form-control-static"><?php echo $rent->rent_age; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">โทรศัพท์</label>
        <p class="form-control-static"><?php echo $rent->rent_phone; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">มือถือ</label>
        <p class="form-control-static"><?php echo $rent->rent_mobile; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">E-Mail</label>
        <p class="form-control-static"><?php echo $rent->rent_email; ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8">
      <div class="form-group ">
        <label for="rent_address" class="control-label">ที่อยู่ปัจจุบัน</label>
        <pre><?php echo $rent->rent_address; ?></pre>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_driver_license_id" class="control-label">เลขที่ใบขับขี่ <span class="required" aria-required="true">*</span></label>
        <p class="form-control-static"><?php echo $rent->rent_driver_license_id; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_issue_driver" class="control-label">ออกโดย</label>
        <p class="form-control-static"><?php echo $rent->rent_issue_driver; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_birthday" class="control-label">วันเกิด</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_birthday); ?></p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_id_card" class="control-label">เลขที่บัตรประชาชน</label>
        <p class="form-control-static"><?php echo $rent->rent_id_card; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_issue_card" class="control-label">ออกโดย</label>
        <p class="form-control-static"><?php echo $rent->rent_issue_card; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label for="rent_end_date_card" class="control-label">วันหมดอายุ</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_end_date_card); ?></p>
      </div>
    </div>
  </div>
</div>
