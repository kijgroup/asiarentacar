<header class="panel-heading">
  6. ข้อมูลการคืนรถ
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">วันที่</label>
        <p class="form-control-static"><?php echo $this->Datetime_service->display_date($rent->rent_end_date); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เวลา</label>
        <p class="form-control-static"><?php echo $rent->rent_end_time; ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label">เลขกิโลเมตร</label>
        <p class="form-control-static"><?php echo $rent->rent_end_km; ?></p>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label">สถานที่</label>
    <p class="form-control-static"><?php echo $rent->rent_end_location; ?></p>
  </div>
</div>
