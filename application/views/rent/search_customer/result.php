<div class="panel panel-default form-horizontal">
  <div class="panel-body">
    <?php
    if($customer_list->num_rows() == 0){
      echo '<div class="text-center"><h3>ไม่พบรายการ</h3></div>';
    }else{
    ?>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>บริษัท</th>
            <th>ข้อมูลลูกค้า</th>
            <th>ข้อมูลติดต่อ</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($customer_list->result() as $customer){ ?>
          <tr>
            <td>
              <div><?php echo $customer->company; ?></div>
              <div>เลขที่เสียภาษี: <?php $customer->tax_id; ?></div>
            </td>
            <td>
              <div><?php echo $customer->pre_name.' '.$customer->first_name.' '.$customer->last_name; ?></div>
              <div>บัตรประชาชน: <?php echo $customer->id_card; ?></div>
              <div>เลขที่ใบขับขี่: <?php echo $customer->driver_license_id; ?></div>
            </td>
            <td>
              <div>เบอร์โทร: <?php echo $customer->phone; ?></div>
              <div>มือถือ: <?php echo $customer->mobile; ?></div>
              <div>อีเมล: <?php echo $customer->email; ?></div>
            </td>
            <td>
              <button
              class="btn btn-primary btn-sel-customer"
              data-customerid="<?php echo $customer->customer_id; ?>"
              data-company="<?php echo $customer->company; ?>"
              data-taxid="<?php echo $customer->tax_id; ?>"
              data-prename="<?php echo $customer->pre_name; ?>"
              data-firstname="<?php echo $customer->first_name; ?>"
              data-lastname="<?php echo $customer->last_name; ?>"
              data-age="<?php echo $customer->age; ?>"
              data-idcard="<?php echo $customer->id_card; ?>"
              data-issuecard="<?php echo $customer->issue_card; ?>"
              data-enddatecard="<?php echo $this->Datetime_service->display_date($customer->end_date_card); ?>"
              data-address="<?php echo $customer->address; ?>"
              data-phone="<?php echo $customer->phone; ?>"
              data-mobile="<?php echo $customer->mobile; ?>"
              data-email="<?php echo $customer->email; ?>"
              data-driverlicenseid="<?php echo $customer->driver_license_id; ?>"
              data-issuedriver="<?php echo $customer->issue_driver; ?>"
              data-birthday="<?php echo $this->Datetime_service->display_date($customer->birthday); ?>"
              >
                เลือก
              </button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <?php } ?>
  </div>
</div>
