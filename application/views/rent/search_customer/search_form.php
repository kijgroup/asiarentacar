<section class="panel">
  <header class="panel-heading">ค้นหาลูกค้า</header>
  <div class="panel-body">
    <div class="form-group">
      <label class="control-label col-md-3">ชื่อบริษัท</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_company" id="search_company" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">เลขประจำตัวผู้เสียภาษี</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_tax_id" id="search_tax_id" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">ชื่อ</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_first_name" id="search_first_name" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">นามสกุล</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_last_name" id="search_last_name" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">โทรศัพท์</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_phone" id="search_phone" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">มือถือ</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_mobile" id="search_mobile" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">E-Mail</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_email" id="search_email" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">เลขที่ใบขับขี่</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_driver_license_id" id="search_driver_license_id" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">เลขที่บัตรประชาชน</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="search_id_card" id="search_id_card" />
      </div>
    </div>
  </div>
  <div class="panel-footer text-right">
    <button class="btn btn-primary" type="button" id="btn-search-customer"><i class="fa fa-search"></i> ค้นหา</button>
    <button class="btn btn-success" type="button" id="btn-new-customer"><i class="fa fa-asterisk"></i> สร้างใหม่</button>
  </div>
</section>
<div id="search-customer-result"></div>
