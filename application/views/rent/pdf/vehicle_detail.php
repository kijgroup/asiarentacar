<div class="outer-border-black no-border-top">
  <div class="border-bottom">
    <table width="100%">
      <col width="46%">
      <col width="54%">
      <tbody>
        <tr>
          <td width="46%" class="label-text text-center"><b>ข้อมูลรถที่เช่า/Details of the Rented Vehicle</b></td>
          <td width="54%" class="label-text text-center"><b>ข้อมูลการเช่า/Peroid of Rental</b></td>
        </tr>
      </tbody>
    </table>
  </div>
  <table width="100%">
    <col width="23%">
    <col width="23%">
    <col width="10%">
    <col width="22%">
    <col width="22%">
    <tbody>
      <tr>
        <td width="23%">
          หมายเลขทะเบียนรถ:<br />
          Registration No.
        </td>
        <td class="border-left" width="23%">
          ยี่ห้อ/รุ่น:<br />
          Make/Model
        </td>
        <td rowspan="2" class="border-bottom border-left text-center darken-cell" valign="middle" width="10%">
          รับรถ<br />
          Rented
        </td>
        <td class="border-bottom border-left" width="22%">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">วันที่:<br />Date</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $this->Datetime_service->display_date($rent->rent_start_date); ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        <td class="border-bottom border-left" width="22%">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">เวลา:<br />Time Out</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_start_time; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td class="border-bottom text-center">
          <b class="data-value" style="font-size:15px;"><?php echo $car->car_registration; ?></b>
        </td>
        <td class="border-bottom border-left text-center">
          <b class="data-value" style="font-size:15px;"><?php echo $carbrand->brand_name.' '.$carmodel->model_name; ?></b>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">สถานที่:<br />Location</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_start_location; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">เลขกิโลเมตร:<br />Kms Out</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_start_km; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          สี:<br />
          Colour
        </td>
        <td class="border-left">
          รหัสคืนรถ:<br />
          Vehicle code
        </td>
        <td rowspan="2" class="border-bottom border-left text-center darken-cell" valign="middle">
          <b>คืนรถ<br />
          Returned</b>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">วันที่:<br />Date</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $this->Datetime_service->display_date($rent->rent_end_date); ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">เวลา:<br />Time Out</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_end_time; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td class="border-bottom text-center">
          <b class="data-value" style="font-size:15px;"><?php echo $car->car_color; ?></b>
        </td>
        <td class="border-bottom border-left text-center">
          <b class="data-value" style="font-size:15px;"><?php echo $carmodel->car_type; ?></b>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">สถานที่:<br />Location</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_end_location; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        <td class="border-bottom border-left">
          <table width="100%">
            <tbody>
              <tr>
                <td width="40%">เลขกิโลเมตร:<br />Kms Out</td>
                <td width="60%" class="text-center" valign="middle">
                  <b class="data-value"><?php echo $rent->rent_end_km; ?></b>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="5" class="border-bottom" style="padding:5px 0;">
          <b>บัตรเครดิต: Credit Card </b>
          <?php echo ($rent->rent_credit_card_id != '')?'<b class="data-value">'.$rent->rent_credit_card_id.'</b>':str_repeat('.', 70); ?>
          <b>วันหมดอายุ: Exp. </b>
          <?php echo ($rent->rent_credit_card_id != '')?'<b class="data-value">'.$this->Datetime_service->display_date($rent->rent_credit_card_end_date).'</b>':str_repeat('.', 45); ?>
        </td>
      </tr>
      <tr>
        <td colspan="5">
          หากน้ำมันไม่เต็มถังในวันคืนรถ ผู้เช่าต้องรับผิดชอบชำระค่าน้ำมันลิตรละ <b class="data-value"><?php echo number_format($rent->rent_oil_price, 2); ?></b> บาท<br />
          If the Vehicle is not returned with a full fuel tank, refilling will be charged at <b class="data-value"><?php echo number_format($rent->rent_oil_price, 2); ?></b> Baht per litre.
        </td>
      </tr>
    </tbody>
  </table>
</div>
