<div class="outer-border-black">
  <div class="label-text text-center border-bottom"><b>ข้อมูลผู้เช่า/The Renter's Details</b></div>
  <table width="100%">
    <tbody>
      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">ชื่อ:<br/>First Name</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_pre_name.' '.$rent->rent_first_name; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-right border-bottom" width="35%">
          <table>
            <tr>
              <td width="50%">นามสกุล:<br/>Last Name</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_last_name; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-bottom" width="25%">
          <table>
            <tr>
              <td width="50%">อายุ:<br/>Age</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_age; ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-bottom" colspan="3">
          <table>
            <tr>
              <td width="20%">ที่อยู่ปัจจุบัน:<br/>Address</td>
              <td width="80%" class="text-left" valign="middle">
                <b class="data-value"><?php echo $rent->rent_address; ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">เลขที่ใบขับขี่:<br/>Driving License No.</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_driver_license_id; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-right border-bottom" width="35%">
          <table>
            <tr>
              <td width="50%">ออกโดย:<br/>Issue by</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_issue_driver; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-bottom" width="25%">
          <table>
            <tr>
              <td width="50%">วันหมดอายุ:<br/>Expiry Date</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $this->Datetime_service->display_date($rent->rent_end_date_card); ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">หมายเลขบัตรประชาชน:<br/>Identification No.</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_id_card; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-right border-bottom" width="35%">
          <table>
            <tr>
              <td width="50%">ออกโดย:<br/>Issue by</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_issue_card; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-bottom" width="25%">
          <table>
            <tr>
              <td width="50%">วัน/เดือน/ปีเกิด:<br/>Date of Birth</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $this->Datetime_service->display_date($rent->rent_birthday); ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">ชื่อผู้ขับร่วม:<br/>Additional Driver Name</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_pre_name.' '.$rent->rent_buddy_first_name; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-right border-bottom" width="35%">
          <table>
            <tr>
              <td width="50%">นามสกุล:<br/>Last Name</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_last_name; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td class="border-bottom" width="25%">
          <table>
            <tr>
              <td width="50%">อายุ:<br/>Age</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_age; ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-bottom" colspan="3">
          <table>
            <tr>
              <td width="20%">ที่อยู่ปัจจุบัน:<br/>Address</td>
              <td width="80%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_address; ?></b>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td style="border-right: 0.5px solid #000;" width="40%">
          <table>
            <tr>
              <td width="50%">เลขที่ใบขับขี่:<br/>Driving License No.</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_driver_license; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td style="border-right: 0.5px solid #000;" width="35%">
          <table>
            <tr>
              <td width="50%">ออกโดย:<br/>Issued by</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $rent->rent_buddy_issue_driver; ?></b>
              </td>
            </tr>
          </table>
        </td>
        <td width="25%">
          <table>
            <tr>
              <td width="50%">วันหมดอายุ:<br/>Expiry Date</td>
              <td width="50%" class="text-center" valign="middle">
                <b class="data-value"><?php echo $this->Datetime_service->display_date($rent->rent_buddy_end_date); ?></td></b>
            </tr>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>
