<div class="outer-border-black no-border-top">
  <div class="label-text text-center border-bottom"><b>รายละเอียดค่าเช่ารถ/Rate Breakdown</b></div>

  <table width="100%">
    <tbody>
      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">ค่าเช่ารถรายวัน วันละ (บาท)<br/>Inclusive Rate (Baht a Day)</td>
              <td width="50%" class="text-center" valign="middle"><b class="data-value"><?php echo number_format($rent->rent_inclusive_rate, 2); ?></b></td>
            </tr>
          </table>
        </td>
        <td class="border-bottom darken-cell" valign="middle" width="20%">
          <table>
            <tr>
              <td class="text-center" width="50%"><b>จำนวนวันที่เช่า (วัน)<br/>Day(s)</b></td>
            </tr>
          </table>
        </td>
        <td class="border-right border-bottom border-left darken-cell" valign="middle" width="20%">
          <table>
            <tr>
              <td class="text-center" width="50%"><b>จำนวนเงิน (บาท)<br/>Baht</b></td>
            </tr>
          </table>
        </td>
        <td class="border-bottom darken-cell" valign="middle" width="20%">
          <table>
            <tr>
              <td class="text-center" width="50%"><b>รวมเป็นเงิน<br/>Total</b></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          จำนวนวันทีเช่า<br/>Day(s) Rented
        </td>
        <td class="border-bottom text-center" width="20%" valign="middle">
          <b class="data-value"><?php echo $rent->rent_totalday; ?></b>
        </td>
        <td class="border-right border-bottom border-left text-center" valign="middle" width="20%">
          <b class="data-value"><?php echo number_format($rent->rent_inclusive_rate, 2); ?></b>
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_total, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          ค่าประกันภัยรถ<br/>LDW
        </td>
        <td class="border-bottom" width="20%">
        </td>
        <td class="border-right border-bottom border-left text-center" valign="middle" width="20%">
          <b class="data-value"><?php echo number_format($rent->rent_total_ldw, 2); ?></b>
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_total_ldw, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          ค่าประกันภัยส่วนบุคคล<br/>PAE
        </td>
        <td class="border-bottom" width="20%">
        </td>
        <td class="border-right border-bottom border-left text-center" valign="middle" width="20%">
          <b class="data-value"><?php echo number_format($rent->rent_total_pae, 2); ?></b>
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_total_pae, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          ค่ารับ/ส่งรถยนต์<br/>Deliver and pickup
        </td>
        <td class="border-bottom" width="20%">
        </td>
        <td class="border-right border-bottom border-left text-center" valign="middle" width="20%">
          <b class="data-value"><?php echo number_format($rent->rent_total_delivery_and_pickup, 2); ?></b>
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_total_delivery_and_pickup, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          ค่าภาษีมูลค่าเพิ่ม 7%<br/>Value Added Tax 7%
        </td>
        <td class="border-bottom" width="20%">
        </td>
        <td class="border-right border-bottom border-left" width="20%">
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_vat, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td class="border-right border-bottom" width="40%">
          <table>
            <tr>
              <td width="50%">ค่าใช้จ่ายอื่นๆ<br/>Others</td>
              <td width="50%" class="text-center" valign="middle"><b class="data-value"><?php echo $rent->rent_note; ?></b></td>
            </tr>
          </table>
        </td>
        <td class="border-bottom" width="20%">
        </td>
        <td class="border-right border-bottom border-left" width="20%">
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_total_other, 2); ?></b>
              </td>
              <td width="10%">บาท<br/>Baht</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="border-bottom" colspan="2" rowspan="2">
          <div style="font-size:5px;">&nbsp;</div>
          ผู้เช่าได้อ่านข้อความ และเงี้อนไขเพิ่มเติมของสัญญาเช่าที่ปรากฎอยู่ทั้งสองด้าน<br/>
          ของสัญญาเช่าฉบับนี้ ขอรับรองว่าเป็นความจริง และตกลงยินยอมปฏิบัติตาม<br/>
          ทุกประการ จึงลงลายมือชื่อไว้เพิ่อเป็นหลักฐาน<br/>
          I have read and agreed on all terms and conditions on both sides of<br/>
          this rental agreement.

        </td>
        <td class="border-left border-right border-bottom text-center darken-cell" valign="middle" width="20%">
          <b>รวมค่าเช่าทั้งสิ้น<br/>Total Charges</b>
        </td>
        <td class="border-bottom" width="20%">
          <table>
            <tr>
              <td width="90%" class="text-center" valign="middle">
                <b class="data-value"><?php echo number_format($rent->rent_grand_total, 2); ?></b>
              </td>
              <td width="10%">บาท<br />Baht</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="border-bottom"><br /><br /><br /><br /></td>
        <td class="border-bottom"></td>
      </tr>
      <tr>
        <td colspan="2"><br/>ลงลายมือชื่อผู้ออกสัญญา X<br/>This rental agreement is initiated by</td>
        <td class="border-left" colspan="2">
          &nbsp;
          <table>
            <tbody>
              <tr>
                <td width="93%">ลงชื่อ: X<br/>Renter's Signature(</td>
                <td width="7%">ผู้เช่า<br/>)</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>
