<div style="padding:10px 0 10px 0;">มีผู้เช่าทั้งหมด <?php echo $rent_list->num_rows(); ?> คน</div>
<section class="panel">
  <table class="table table-hover">
    <thead>
      <tr>
         <th>#</th>
         <th>เลขที่สัญญา</th>
         <th>ผู้เช่า</th>
         <th>ทะเบียนรถ</th>
         <th>วันเริ่มเช่า</th>
         <th>สิ้นสุด</th>
         <th>รวมค่าเช่าทั้งหมด</th>
      </tr>
    </thead>
    <tbody>
      <?php
        foreach($rent_list -> result() as $rent){
          $detail_url = 'rent/detail/'.$rent->rent_id;
      ?>
      <tr>
        <td><?php echo $rent->rent_id; ?></td>
        <td><?php echo anchor($detail_url, $rent->rent_code); ?></td>
        <td><?php echo anchor($detail_url, $rent->rent_pre_name.' '.$rent->rent_first_name.' '.$rent->rent_last_name); ?></td>
        <td><?php echo anchor($detail_url, $this->Car_model->get_name($rent->car_id)); ?></td>
        <td><?php echo anchor($detail_url, $this->Datetime_service->display_date($rent->rent_start_date)); ?></td>
        <td><?php echo anchor($detail_url, $this->Datetime_service->display_date($rent->rent_end_date)); ?></td>
        <td><?php echo anchor($detail_url, $rent->rent_grand_total); ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</section>
<?php
$total_page = ceil($total_transaction/$per_page);
$this->load->view('rent/list/pager', array(
  'total_transaction' => $total_transaction,
  'search_val' => $search_val,
  'per_page' => $per_page,
  'page' => $page,
  'total_page' => $total_page,
  'action' => site_url('rent/filter/'),
));
