<?php
$total_page = ceil($total_transaction/$per_page);
$this->load->view('customer/list/pager', array(
  'total_transaction' => $total_transaction,
  'search_val' => $search_val,
  'per_page' => $per_page,
  'page' => $page,
  'total_page' => $total_page,
  'action' => site_url('customer/filter/'),
));
