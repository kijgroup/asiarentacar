<h3 class="page-header"><i class="fa fa-file-text"></i> สัญญาเช่ารถยนต์</h3>
<div class="search-control-wrapper navbar-form" style="padding-bottom:10px;">
  <?php echo form_open('rent/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
  <div class="form-group">
    <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
  </div>
  <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-search"></i> ค้นหา</button>
  <?php echo form_close(); ?>
</div>
<hr/>
<div style="padding-bottom:10px;">
  <?php echo anchor('rent/form','<i class="fa fa-plus"></i> เพิ่มสัญญาเช่ารถยนต์', array('class'=>'btn btn-success',  'title'=>'สัญญาเช่ารถยนต์')); ?>
</div>
<div id="result">
  Loading
</div>
