<h3 class="page-header"><i class="fa fa-car"></i> ข้อมูลรถ</h3>
<div class="row" style="margin-bottom:20px; margin-left:16px;">
  <div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> การกระทำ <span class="caret"></span> </button>
    <ul class="dropdown-menu">
      <li><?php echo anchor('car/form/'.$car_id, 'แก้ไขข้อมูล');?></li>
      <li><?php echo anchor('car/form/0', 'เพิ่มข้อมูล');?></li>
      <li class="divider"></li>
      <li><?php echo anchor('car/delete/'.$car_id,'ลบข้อมูล'); ?></li>
    </ul>
  </div>
  <?php echo anchor('car/index','กลับไปหน้ารายการ', array('class'=>'btn btn-danger')); ?>
</div>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลรถ
    </header>
    <div class="panel-body">
      <div class="form-horizontal">
        <div class="form-group">
          <label class="control-label col-lg-2">ยี่ห้อรถ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $brand_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">รุ่นรถ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $model_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ทะเบียนรถ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $car->car_registration; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">สีรถ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $car->car_color; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Created : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($car->create_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($car->create_by); ?></p>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Updated : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($car->update_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($car->update_by); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
