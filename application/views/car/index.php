<h3 class="page-header"><i class="fa fa-car"></i> ข้อมูลรถ</h3>
<div class="search-control-wrapper navbar-form" style="padding-bottom:10px;">
  <form role="form" class="form-inline" id="searchForm" method="get" accept-charset="utf-8">
    <div class="form-group">
      <input type="text" class="form-control" name="carcode" id="carcode" placeholder="ทะเบียนรถ" value="<?php echo $search_carcode; ?>">
    </div>
    <div class="form-group">
      <select class="form-control" name="carbrand" id="carbrand">
        <option value="all">ยี่ห้อรถทั้งหมด</option>
        <?php
        $brand_list = $this->Carbrand_model->get_list();
        foreach($brand_list->result() as $brand){
          $selected = ($search_brand == $brand->brand_id)?'selected="selected"':'';
          echo '<option value="'.$brand->brand_id.'" '.$selected.'>'.$brand->brand_name.'</option>';
        }
        ?>
      </select>
    </div>
    <div class="form-group">
      <select class="form-control" name="carmodel" id="carmodel">
        <option value="all">รุ่นรถทั้งหมด</option>
        <?php
        $model_list = $this->Carmodel_model->get_list();
        foreach($model_list->result() as $model){
          $selected = ($search_model == $model->model_id)?'selected="selected"':'';
          echo '<option value="'.$model->model_id.'" '.$selected.' class="model-brand model-brand-'.$model->brand_id.'">'.$model->model_name.'</option>';
        }
        ?>
      </select>
    </div>
    <div class="form-group">
      <input type="text" class="form-control" name="color" id="color" placeholder="สีรถ" value="<?php echo $search_color; ?>">
    </div>
    <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-search"></i> ค้นหา</button>
  </form>
</div>
<hr/>
<div>
  <?php echo anchor('car/form','<i class="fa fa-plus"></i> เพิ่มรถ', array('class'=>'btn btn-success', 'title'=>'Bootstrap 3 themes generator')); ?>
</div>
<div style="padding:10px 0 10px 0;">มีรถทั้งหมด <?php echo $car_list->num_rows(); ?> คัน</div>
<section class="panel">
  <table class="table table-hover">
    <thead>
      <tr>
        <th width="40">#</th>
        <th>ทะเบียนรถ</th>
        <th>ยี่ห้อรถ</th>
        <th>รุ่นรถ</th>
        <th>สีรถ</th>
      </tr>
    </thead>
    <tbody>
    <?php
      foreach($car_list->result() as $car){
        $detail_url = 'car/detail/'.$car->car_id;
        $carmodel = $this->Carmodel_model->get_data($car->model_id);
      ?>
      <tr>
        <td><?php echo $car->car_id; ?></td>
        <td><?php echo anchor($detail_url, $car->car_registration); ?></td>
        <td><?php echo anchor($detail_url, $this->Carbrand_model->get_name($car->brand_id)); ?></td>
        <td><?php echo anchor($detail_url, $this->Carmodel_model->get_name($car->model_id)); ?></td>
        <td><?php echo anchor($detail_url, $car->car_color); ?></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
</section>
