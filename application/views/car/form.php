<h3 class="page-header"><i class="fa fa-car"></i> ข้อมูลรถ</h3>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
    ข้อมูลรถ
    </header>
    <?php echo form_open('car/form_post/'. $car_id, array('class'=>'form-validate form-horizontal')); ?>
      <div class="panel-body">
        <div class="form-group">
          <label class="control-label col-lg-3">ยี่ห้อรถ <span class="required">*</span></label>
          <div class="col-lg-6">
            <select class="form-control" name="ddl_brand" id="ddl_brand">
              <?php
                $carbrand_list = $this->Carbrand_model->get_list();
                foreach($carbrand_list->result() as $carbrand){
                  $selected = ($car_id != 0 && $carbrand->brand_id == $carmodel->brand_id)?'selected="selected"':'';
                  echo '<option value="'.$carbrand->brand_id.'" '.$selected.'>'.$carbrand->brand_name.'</option>';
                }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-3">รุ่นรถ <span class="required">*</span></label>
          <div class="col-lg-6">
            <select class="form-control" name="ddl_model" id="ddl_model">
              <option value="">โปรดระบุ</option>
              <?php
                $model_list = $this->Carmodel_model->get_list();
                foreach($model_list->result() as $model){
                  $selected = ($model->model_id == $car->model_id)?'selected="selected"':'';
                  echo '<option value="'.$model->model_id.'" '.$selected.' class="model-brand model-brand-'.$model->brand_id.'">'.$model->model_name.'</option>';
                }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-3">ทะเบียนรถ <span class="required">*</span></label>
          <div class="col-lg-6">
          <input type="text" class="form-control" name="car_registration" value="<?php echo($car_id !=0)? $car->car_registration:''; ?>"/>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-3">สีรถ <span class="required">*</span></label>
          <div class="col-lg-6">
          <input type="text" class="form-control" name="car_color" value="<?php echo($car_id !=0)? $car->car_color:''; ?>"/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-offset-3 col-lg-10">
            <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
            <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
          </div>
        </div>
      </div>
    <?php echo form_close(); ?>
  </section>
</div>
