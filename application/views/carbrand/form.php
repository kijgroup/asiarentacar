<h3 class="page-header"><i class="fa fa-building-o"></i> ข้อมูลยี่ห้อรถ</h3>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
    ข้อมูลยี่ห้อรถ
    </header>
    <?php echo form_open('carbrand/form_post/'. $brand_id, array('class'=>'form-validate form-horizontal')); ?>
    <div class="panel-body">
      <div class="form">
          <div class="form-group ">
            <label class="control-label col-lg-3">ยี่ห้อรถ <span class="required">*</span></label>
            <div class="col-lg-6">
            <input type="text" class="form-control" name="carbrand" value="<?php echo($brand_id !=0)? $brand->brand_name:''; ?>"/>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
              <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
              <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
            </div>
          </div>
        </div>
      </div>
    <?php echo form_close(); ?>
  </section>
</div>
