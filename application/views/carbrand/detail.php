<h3 class="page-header"><i class="fa fa-building-o"></i> ข้อมูลยี่ห้อรถ</h3>
<div class="row" style="margin-bottom:20px; margin-left:16px;">
  <div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> การกระทำ <span class="caret"></span> </button>
    <ul class="dropdown-menu">
      <li><?php echo anchor('carbrand/form/'.$brand_id, 'แก้ไขข้อมูล');?></li>
      <li><?php echo anchor('carbrand/form/0', 'เพิ่มข้อมูล');?></li>
      <li class="divider"></li>
      <li><?php echo anchor('carbrand/delete/'.$brand_id,'ลบข้อมูล'); ?></li>
    </ul>
  </div>
  <?php echo anchor('carbrand/index','กลับไปหน้ารายการ', array('class'=>'btn btn-danger')); ?>
</div>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลยี่ห้อรถ
    </header>
    <div class="panel-body">
      <div class="form-horizontal">
        <div class="form-group">
          <label class="control-label col-lg-2">ยี่ห้อรถ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $brand->brand_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Created : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($brand->create_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($brand->create_by); ?></p>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Updated : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($brand->update_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($brand->update_by); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
