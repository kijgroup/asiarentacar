<h3 class="page-header"><i class="fa fa-building-o"></i> ข้อมูลยี่ห้อรถ</h3>
<div>
<?php echo anchor('carbrand/form','<i class="fa fa-plus"></i> เพิ่มยี่ห้อรถ', array('class'=>'btn btn-success', 'title'=>'Bootstrap 3 themes generator')); ?>
</div>

<div style="padding:10px 0 10px 0;">มียี่ห้อรถทั้งหมด <?php echo $brand_list->num_rows(); ?> ยี่ห้อ</div>

  <section class="panel">
    <table class="table table-hover">
      <thead>
        <tr>
          <th width="40">#</th>
          <th>ยี่ห้อรถ</th>
        </tr>
      </thead>
      <tbody>
      <?php
        foreach($brand_list->result() as $brand){
          $detail_url = 'carbrand/detail/'.$brand->brand_id;
        ?>
        <tr>
          <td><?php echo $brand->brand_id; ?></td>
          <td><?php echo anchor($detail_url, $brand->brand_name); ?></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </section>
