<h3 class="page-header"><i class="fa fa-group"></i>ข้อมูลเจ้าหน้าที่</h3>
<div class="row" style="margin-bottom:20px; margin-left:16px;">
  <div class="btn-group">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> การกระทำ <span class="caret"></span> </button>
    <ul class="dropdown-menu">
      <li><?php echo anchor('admin/form/'.$admin_id, 'แก้ไขข้อมูล');?></li>
      <li><?php echo anchor('admin/form/0', 'เพิ่มข้อมูล');?></li>
      <li class="divider"></li>
      <li><?php echo anchor('admin/delete/'.$admin_id,'ลบข้อมูล'); ?></li>
    </ul>
  </div>
  <?php echo anchor('admin/index','กลับไปหน้ารายการ', array('class'=>'btn btn-danger')); ?>
</div>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลเจ้าหน้าที่
    </header>
    <div class="panel-body">
      <div class="form-horizontal">
        <div class="form-group">
          <label class="control-label col-lg-2">Username : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $admin->username; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">ชื่อผู้ใช้ : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $admin->admin_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Status : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $admin->status; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Created : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($admin->create_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($admin->create_by); ?></p>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Updated : </label>
          <div class="col-lg-10">
            <p class="form-control-static"><?php echo $this->Datetime_service->display_datetime($admin->update_date); ?></p>
            <div>
              <p class="form-control-static"><b>By : </b><?php echo $this->Admin_model->get_name($admin->update_by); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
