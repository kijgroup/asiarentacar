<h3 class="page-header"><i class="fa fa-group"></i><?php echo ($admin_id == 0)?'เพิ่มเจ้าหน้าที่':'แก้ไขข้อมูลเจ้าหน้าที่'; ?></h3>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลเจ้าหน้าที่
    </header>
    <?php echo form_open('admin/form_post/'.$admin_id, array('class'=>'form-validate form-horizontal')); ?>
      <div class="panel-body">
        <?php
        if($err_msg != ''){
          echo '<div class="alert alert-block alert-danger fade in"> Password ไม่ถูกต้อง </div>';
        }
        ?>
        <div class="form">
          <div class="form-group ">
            <label class="control-label col-lg-3">Username <span class="required">*</span></label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="username" value="<?php echo($admin_id !=0)? $admin->username:''; ?>"/>
            </div>
          </div>
          <?php if($admin_id == 0){ ?>
          <div class="form-group ">
            <label class="control-label col-lg-3">Password <span class="required">*</span></label>
            <div class="col-lg-6">
                <input type="password" class="form-control" name="password" required="">
            </div>
          </div>
          <div class="form-group ">
            <label class="control-label col-lg-3">Confirm Password <span class="required">*</span></label>
            <div class="col-lg-6">
                <input type="password" class="form-control" name="confirmpassword" required="">
            </div>
          </div>
          <?php } ?>
          <div class="form-group ">
            <label for="ccomment" class="control-label col-lg-3">ชื่อผู้ใช้ <span class="required">*</span></label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="admin_name" required="" value="<?php echo($admin_id !=0)? $admin->admin_name:''; ?>"/>
            </div>
          </div>
          <div class="form-group ">
            <label for="ccomment" class="control-label col-lg-3">Status <span class="required">*</span></label>
            <div class="col-lg-6">

              <label class="label_radio r_on">
                <input type="radio" name="sample-radio" id="radio-01" value="active" <?php echo ($admin_id == 0 OR $admin->status == 'active')?'checked':''; ?>> ใช้งานได้
              </label>

            </div>
            <div class="col-lg-6">
              <label class="label_radio r_on">
                <input type="radio" name="sample-radio" id="radio-01" value="disable" <?php echo ($admin_id !=0 && $admin->status == 'disable')?'checked':''; ?>> ปิดการใช้งาน
              </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
              <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
              <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
            </div>
          </div>
        </div>
      </div>
    <?php echo form_close(); ?>
  </section>
</div>
