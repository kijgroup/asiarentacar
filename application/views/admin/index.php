<h3 class="page-header"><i class="fa fa-group"></i> ข้อมูลเจ้าหน้าที่</h3>
<div>
  <?php echo anchor('admin/form','<i class="fa fa-plus"></i> เพิ่มผู้ใช้', array('class'=>'btn btn-success', 'title'=>'Bootstrap 3 themes generator')); ?>
</div>
<div style="padding:10px 0 10px 0;">มีเจ้าหน้าที่ทั้งหมด <?php echo $admin_list->num_rows(); ?> คน</div>
  <section class="panel">
    <table class="table table-hover">
      <thead>
        <tr>
          <th width="40">#</th>
          <th>Username</th>
          <th>ชื่อผู้ใช้</th>
          <th>สถานะ</th>
          <th>วันที่สร้าง</th>
        </tr>
      </thead>
      <tbody>
      <?php
        foreach($admin_list->result() as $admin){
          $detail_url = 'admin/detail/'.$admin->admin_id;
        ?>
        <tr>
          <td><?php echo $admin->admin_id; ?></td>
          <td><?php echo anchor($detail_url, $admin->username); ?></td>
          <td><?php echo anchor($detail_url, $admin->admin_name); ?></td>
          <td><?php echo ($admin->status == 'active')?'<span class="label label-info">ใช้งานได้</span>':'<span class="label label-danger">ปิดการใช้งาน</span>'; ?></td>
          <td><?php echo $this->Datetime_service->display_datetime($admin->create_date); ?></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </section>
