<h3 class="page-header"><i class="fa fa-lock"></i>เปลี่ยนรหัสผ่าน</h3>

<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลเจ้าหน้าที่
    </header>
    <?php echo form_open('admin/changepassword_post/'.$admin_id, array('class'=>'form-validate form-horizontal')); ?>
      <div class="panel-body">
        <?php
        if($err_msg != ''){

          echo '<div class="alert alert-block alert-danger fade in"> Password ไม่ถูกต้อง </div>';
        }
        ?>
        <div class="form">
          <div class="form-group ">
            <label class="control-label col-lg-2">Username <span class="required">*</span></label>
            <div class="col-lg-10">
                <p class="form-control"><span class="required"><?php echo $admin->username; ?></span></p>
            </div>
          </div>

          <div class="form-group ">
            <label class="control-label col-lg-2">New Password <span class="required">*</span></label>
            <div class="col-lg-10">
                <input type="password" class="form-control" name="newpassword" required="">
            </div>
          </div>
          <div class="form-group ">
            <label class="control-label col-lg-2">Confirm Password <span class="required">*</span></label>
            <div class="col-lg-10">
                <input type="password" class="form-control" name="confirmpassword" required="">
            </div>
          </div>

          <div class="form-group ">
            <label for="ccomment" class="control-label col-lg-2">ชื่อผู้ใช้ <span class="required">*</span></label>
            <div class="col-lg-10">
                <p class="form-control"><span class="required"><?php echo $admin->admin_name; ?></span></p>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
              <button type="submit" class="btn btn-primary">Save</button>
              <button type="button" class="btn btn-default" onclick="history.go(-1);">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    <?php echo form_close(); ?>
  </section>
</div>
