<h3 class="page-header"><i class="fa fa-tachometer"></i> ข้อมูลรุ่นรถ</h3>
<div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading">
      ข้อมูลรุ่นรถ
    </header>
    <?php echo form_open('carmodel/form_post/'.$model_id, array('class'=>'form-validate form-horizontal')); ?>
    <div class="panel-body">
      <div class="form-group ">
        <label class="control-label col-lg-3">รุ่นรถ <span class="required">*</span></label>
        <div class="col-lg-6">
        <input type="text" class="form-control" name="txtName" value="<?php echo($model_id !=0)? $model->model_name:''; ?>"/>
        </div>
      </div>
      <div class="form-group ">
        <label class="control-label col-lg-3">รหัสรถ <span class="required">*</span></label>
        <div class="col-lg-6">
        <input type="text" class="form-control" name="car_type" id="car_type" value="<?php echo($model_id !=0)? $model->car_type:''; ?>" placeholder="E-CAR, C-CAR, F-CAR, MVMR" />
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-lg-3" for="inputSuccess">ยี่ห้อรถ</label>
        <div class="col-lg-6">
          <select class="form-control m-bot15" name="ddlBrand" id="ddlBrand">
            <?php
              $carbrand_list = $this->Carbrand_model->get_list();
              foreach($carbrand_list->result() as $carbrand){
                echo '<option value="'.$carbrand->brand_id.'">'.$carbrand->brand_name.'</option>';
              }
            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
          <button class="btn btn-primary" type="submit"><i class="fa fa-download"></i> Save</button>
          <button class="btn btn-danger" type="button" onclick="history.go(-1);"><i class="fa fa-times"></i> Cancel</button>
        </div>
      </div>
    </div>
    <?php echo form_close(); ?>
  </section>
</div>
