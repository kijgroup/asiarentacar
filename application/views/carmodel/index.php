<h3 class="page-header"><i class="fa fa-tachometer"></i> ข้อมูลรุ่นรถ</h3>
<div class="search-control-wrapper navbar-form" style="padding-bottom:10px;">
  <form role="form" class="form-inline" id="searchForm" method="get" accept-charset="utf-8">
    <div class="form-group">
      <select class="form-control" name="carbrand" id="carbrand">
        <option value="all">ยี่ห้อรถทั้งหมด</option>
        <?php
        $brand_list = $this->Carbrand_model->get_list();
        foreach($brand_list->result() as $brand){
          $selected = ($search_brand == $brand->brand_id)?'selected="selected"':'';
          echo '<option value="'.$brand->brand_id.'" '.$selected.'>'.$brand->brand_name.'</option>';
        }
        ?>
      </select>
    </div>
    <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-search"></i> ค้นหา</button>
  </form>
</div>
<hr/>
<div>
<?php echo anchor('carmodel/form','<i class="fa fa-plus"></i> เพิ่มรุ่นรถ', array('class'=>'btn btn-success', 'title'=>'Bootstrap 3 themes generator')); ?>
</div>
<div style="padding:10px 0 10px 0;">มีรุ่นรถทั้งหมด <?php echo $model_list->num_rows(); ?> รุ่น</div>
<section class="panel">
  <table class="table table-hover">
    <thead>
      <tr>
        <th width="40">#</th>
        <th>รหัสรถ</th>
        <th>รุ่นรถ</th>
        <th>ยี่ห้อรถ</th>
      </tr>
    </thead>
    <tbody>
    <?php
      foreach($model_list->result() as $model){
        $detail_url = 'carmodel/detail/'.$model->model_id;
      ?>
      <tr>
        <td><?php echo $model->model_id; ?></td>
        <td><?php echo $model->car_type; ?></td>
        <td><?php echo anchor($detail_url, $model->model_name); ?></td>
        <td><?php echo $this->Carbrand_model->get_name($model->brand_id); ?></td>
      </tr>
    <?php } ?>
    </tbody>
  </table>
</section>
