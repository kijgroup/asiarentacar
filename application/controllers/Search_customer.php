<?php
class Search_customer extends CI_Controller{
  public function index(){
    $this->load->model(array('customer/Customer_search_model'));
    $data = array(
      'search_company' => trim($this->input->post('search_company')),
      'search_tax_id' => trim($this->input->post('search_tax_id')),
      'search_first_name' => trim($this->input->post('search_first_name')),
      'search_last_name' => trim($this->input->post('search_last_name')),
      'search_phone' => trim($this->input->post('search_phone')),
      'search_mobile' => trim($this->input->post('search_mobile')),
      'search_email' => trim($this->input->post('search_email')),
      'search_driver_license_id' => trim($this->input->post('search_driver_license_id')),
      'search_id_card' => trim($this->input->post('search_id_card')),
      'search_category' => trim($this->input->post('search_category'))
    );
    $this->load->view('rent/search_customer/result', $this->Customer_search_model->search_data($data));
  }
}
