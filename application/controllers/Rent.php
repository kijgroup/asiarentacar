<?php
class Rent extends CI_Controller{
  var $SEL_NAV = 'rent';
  public function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array(
      'rent/Rent_model',
      'carbrand/Carbrand_model',
      'carmodel/Carmodel_model',
      'car/Car_model',
      'customer/Customer_model'
    ));
  }
  public function index(){
    $this->Login_service->must_login();
    $title = 'สัญญาเช่ารถยนต์';
    $content = $this->load->view('rent/index', array(), true);
    $this->Masterpage_service->add_js('assets/js/page/rent/list.js');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('rent/Rent_filter_service');
    $data = $this->Rent_filter_service->get_data_content(
      $search_val,
      $page,
      $per_page);
    $this->load->view('rent/list/content', $data);
  }
  public function form($rent_id = 0){
    $rent = $this->Rent_model->get_data($rent_id);
    $car = (!$rent)?false:$this->Car_model->get_data($rent->car_id);
    $carmodel = (!$car)?false:$this->Carmodel_model->get_data($car->model_id);
    $carbrand = (!$carmodel)?false:$this->Carbrand_model->get_data($carmodel->brand_id);
    $customer = (!$rent)?false:$this->Customer_model->get_data($rent->customer_id);
    $dataContent = array(
      'rent_id' => $rent_id,
      'rent' => $rent,
      'car' => $car,
      'customer' => $customer,
      'carmodel' => $carmodel,
      'carbrand' => $carbrand
    );
    $content = $this->load->view('rent/form', $dataContent, true);
    $title = ($rent_id == 0)? 'เพิ่มสัญญาเช่ารถยนต์':'แก้ไขสัญญาเช่ารถยนต์';
    $this->Masterpage_service->add_js('assets/js/page/rent/form.js?v=1');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($rent_id){
    $data =  array(
      'customer_id' => $this->input->post('customer_id'),
      'rent_code' => $this->input->post('rent_code'),
      'rent_date' => $this->input->post('rent_date'),
      'rent_company' => $this->input->post('rent_company'),
      'rent_tax_id' => $this->input->post('rent_tax_id'),
      'rent_pre_name' => $this->input->post('rent_pre_name'),
      'rent_first_name' => $this->input->post('rent_first_name'),
      'rent_last_name' => $this->input->post('rent_last_name'),
      'rent_age' => $this->input->post('rent_age'),
      'rent_phone' => $this->input->post('rent_phone'),
      'rent_mobile' => $this->input->post('rent_mobile'),
      'rent_email' => $this->input->post('rent_email'),
      'rent_address' => $this->input->post('rent_address'),
      'rent_driver_license_id' => $this->input->post('rent_driver_license_id'),
      'rent_issue_driver' => $this->input->post('rent_issue_driver'),
      'rent_birthday' => $this->input->post('rent_birthday'),
      'rent_id_card' => $this->input->post('rent_id_card'),
      'rent_issue_card' => $this->input->post('rent_issue_card'),
      'rent_end_date_card' => $this->input->post('rent_end_date_card'),
      'rent_buddy_pre_name' => $this->input->post('rent_buddy_pre_name'),
      'rent_buddy_first_name' => $this->input->post('rent_buddy_first_name'),
      'rent_buddy_last_name' => $this->input->post('rent_buddy_last_name'),
      'rent_buddy_age' => $this->input->post('rent_buddy_age'),
      'rent_buddy_phone' => $this->input->post('rent_buddy_phone'),
      'rent_buddy_mobile' => $this->input->post('rent_buddy_mobile'),
      'rent_buddy_address' => $this->input->post('rent_buddy_address'),
      'rent_buddy_driver_license' => $this->input->post('rent_buddy_driver_license'),
      'rent_buddy_issue_driver' => $this->input->post('rent_buddy_issue_driver'),
      'rent_buddy_end_date' => $this->input->post('rent_buddy_end_date'),
      'car_id' => $this->input->post('car'),
      'rent_start_date' => $this->input->post('rent_start_date'),
      'rent_start_time' => $this->input->post('rent_start_time'),
      'rent_start_km' => $this->input->post('rent_start_km'),
      'rent_start_location' => $this->input->post('rent_start_location'),
      'rent_end_date' => $this->input->post('rent_end_date'),
      'rent_end_time' => $this->input->post('rent_end_time'),
      'rent_end_km' => $this->input->post('rent_end_km'),
      'rent_end_location' => $this->input->post('rent_end_location'),
      'rent_totalday' => $this->input->post('rent_totalday'),
      'rent_inclusive_rate' => $this->input->post('rent_inclusive_rate'),
      'rent_total' => $this->input->post('rent_total'),
      'rent_total_ldw' => $this->input->post('rent_total_ldw'),
      'rent_total_pae' => $this->input->post('rent_total_pae'),
      'rent_total_delivery_and_pickup' => $this->input->post('rent_total_delivery_and_pickup'),
      'rent_total_other' => $this->input->post('rent_total_other'),
      'rent_sub_total' => $this->input->post('rent_sub_total'),
      'rent_vat' => $this->input->post('rent_vat'),
      'rent_grand_total' => $this->input->post('rent_grand_total'),
      'rent_oil_price' => $this->input->post('rent_oil_price'),
      'rent_payment' => $this->input->post('rent_payment'),
      'rent_type_credit_card' => $this->input->post('rent_type_credit_card'),
      'rent_credit_card_id' => $this->input->post('rent_credit_card_id'),
      'rent_credit_card_end_date' => $this->input->post('rent_credit_card_end_date'),
      'rent_company_bill' => $this->input->post('rent_company_bill'),
      'rent_bill_address' => $this->input->post('rent_bill_address'),
      'rent_bill_credit_date' => $this->input->post('rent_bill_credit_date'),
      'rent_payment_other' => $this->input->post('rent_payment_other'),
      'rent_note' => $this->input->post('rent_note'),
    );
    $data['rent_date'] = $this->Datetime_service->convert_to_db($data['rent_date']);
    $data['rent_birthday'] = $this->Datetime_service->convert_to_db($data['rent_birthday']);
    $data['rent_end_date_card'] = $this->Datetime_service->convert_to_db($data['rent_end_date_card']);
    $data['rent_buddy_end_date'] = $this->Datetime_service->convert_to_db($data['rent_buddy_end_date']);
    $data['rent_start_date'] = $this->Datetime_service->convert_to_db($data['rent_start_date']);
    $data['rent_end_date'] = $this->Datetime_service->convert_to_db($data['rent_end_date']);
    if($data['customer_id'] == 0){
      $customer_id = $this->Customer_model->insert(array(
        'company' => $data['rent_company'],
        'category' => $this->input->post('category'),
        'tax_id' => $data['rent_tax_id'],
        'pre_name' => $data['pre_name'],
        'first_name' => $data['rent_first_name'],
        'last_name' => $data['rent_last_name'],
        'age' => $data['rent_age'],
        'id_card' => $data['rent_id_card'],
        'issue_card' => $data['rent_issue_card'],
        'end_date_card' => $data['rent_end_date_card'],
        'address' => $data['rent_address'],
        'phone' => $data['rent_phone'],
        'mobile' => $data['rent_mobile'],
        'email' => $data['rent_email'],
        'driver_license_id' => $data['rent_driver_license_id'],
        'issue_driver' => $data['rent_issue_driver'],
        'birthday' => $data['rent_birthday']
      ));
      $data['customer_id'] = $customer_id;
    }
    if ($rent_id == 0){
      $rent_id = $this->Rent_model->insert($data);
    }else{
      $this->Rent_model->update($rent_id, $data);
    }
    redirect('rent/detail/'.$rent_id);
  }
  public function detail($rent_id){
    $rent = $this->Rent_model->get_data($rent_id);
    $car = $this->Car_model->get_data($rent->car_id);
    $carmodel = (!$car)?false:$this->Carmodel_model->get_data($car->model_id);
    $carbrand = (!$carmodel)?false:$this->Carbrand_model->get_data($carmodel->brand_id);
    $dataContent = array(
      'rent_id' => $rent_id,
      'rent' => $rent,
      'car' => $car,
      'carmodel' => $carmodel,
      'carbrand' => $carbrand
    );
    $content = $this->load->view('rent/detail', $dataContent, true);
    $title = 'ข้อมูลสัญญาเช่ารถยนต์';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function delete($rent_id){
    $this->Rent_model->delete($rent_id);
    redirect('rent');
  }
  public function print_pdf($rent_id){
    $this->Login_service->must_login();
    $rent = $this->Rent_model->get_data($rent_id);
    $car = $this->Car_model->get_data($rent->car_id);
    $carmodel = (!$car)?false:$this->Carmodel_model->get_data($car->model_id);
    $carbrand = (!$carmodel)?false:$this->Carbrand_model->get_data($carmodel->brand_id);
    $title = 'ข้อมูลสัญญาเช่ารถยนต์';
    $html = $this->load->view('rent/pdf', array(
      'rent_id' => $rent_id,
      'rent' => $rent,
      'car' => $car,
      'carmodel' => $carmodel,
      'carbrand' => $carbrand,
      'title' => $title
    ), TRUE);
    $stylesheet = file_get_contents(FCPATH.'assets/css/pdf.css');
    $file_name = 'rent_'.$rent->rent_id.'.pdf';
    //var_dump($file_name);
    $this->load->model(array('Pdf_service'));
    //echo $html;
    $this->Pdf_service->view_pdf_file($html, $stylesheet, '', '', $file_name);
  }
}
