<?php
class Carmodel extends CI_Controller{
  var $SEL_NAV = 'carmodel';
  public function __construct(){
    parent::__construct();
      $this->Login_service->must_login();
      $this->load->model(array('carmodel/Carmodel_model', 'carbrand/Carbrand_model'));
  }
  public function index(){
    $search_brand = (isset($_GET['carbrand']))?$this->input->get('carbrand'):'all';
    $dataContent = array(
      'model_list' => $this->Carmodel_model->get_list($search_brand),
      'search_brand' => $search_brand
    );
    $content = $this->load->view('carmodel/index', $dataContent, true);
    $title = 'ข้อมูลรุ่นรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form($model_id = 0){
    $dataContent = array(
      'model_id' => $model_id,
      'model' => $this->Carmodel_model->get_data($model_id)
    );
    $content = $this->load->view('carmodel/form', $dataContent, true);
    $title = ($model_id == 0)?'เพิ่มรุ่นรถ':'แก้ไขข้อมูลรุ่นรถ';
    $this->Masterpage_service->add_js('assets/js/page/carmodel/form.js?v=1');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($model_id){
    $data = array(
      'brand_id' => $this->input->post('ddlBrand'),
      'model_name' => $this->input->post('txtName'),
      'car_type' => $this->input->post('car_type'),
    );
    if($model_id == 0){
      $model_id = $this->Carmodel_model->insert($data);
    }else{
      $this->Carmodel_model->update($model_id, $data);
    }
    redirect('carmodel/detail/'. $model_id);
  }
  public function detail($model_id){
    $dataContent = array(
      'model_id' => $model_id,
      'model' => $this->Carmodel_model->get_data($model_id)
    );
    $content = $this->load->view('carmodel/detail', $dataContent, true);
    $title = 'ข้อมูลรุ่นรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function delete($model_id){
    $this->Carmodel_model->delete($model_id);
    redirect('carmodel');
  }
}
