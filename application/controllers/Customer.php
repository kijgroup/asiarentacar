<?php
class Customer extends CI_Controller{
  var $SEL_NAV = 'customer';
  public function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model('customer/Customer_model');
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('customer/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets/js/page/customer/list.js');
    $this->Masterpage_service->display($content, 'ลูกค้า', $this->SEL_NAV);
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('customer/Customer_filter_service');
    $data = $this->Customer_filter_service->get_data_content(
      $search_val,
      $page,
      $per_page);
    $this->load->view('customer/list/content', $data);
  }
  public function form($customer_id = 0){
    $customer = $this->Customer_model->get_data($customer_id);
    $title = (($customer_id == 0)?'เพิ่ม':'แก้ไข').'ข้อมูลลูกค้า';
    $content = $this->load->view('customer/form', array(
      'customer_id' => $customer_id,
      'customer' => $customer,
      'title' => $title
    ), true);
    $this->Masterpage_service->add_js('/assets/js/page/customer/form.js');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($customer_id){
    $data = array(
      'company' => $this->input->post('company'),
      'tax_id' => $this->input->post('tax_id'),
      'pre_name' => $this->input->post('pre_name'),
      'first_name' => $this->input->post('first_name'),
      'last_name' => $this->input->post('last_name'),
      'age' => $this->input->post('age'),
      'id_card' => $this->input->post('id_card'),
      'issue_card' => $this->input->post('issue_card'),
      'end_date_card' => $this->input->post('end_date_card'),
      'address' => $this->input->post('address'),
      'phone' => $this->input->post('phone'),
      'mobile' => $this->input->post('mobile'),
      'email' => $this->input->post('email'),
      'driver_license_id' => $this->input->post('driver_license_id'),
      'issue_driver' => $this->input->post('issue_driver'),
      'birthday' => $this->input->post('birthday'),
      'category' => $this->input->post('category')
    );
    $data['birthday'] = $this->Datetime_service->convert_to_db($data['birthday']);
    $data['end_date_card'] = $this->Datetime_service->convert_to_db($data['end_date_card']);
    if ($customer_id == 0){
      $customer_id = $this->Customer_model->insert($data);
    }else{
      $this->Customer_model->update($customer_id, $data);
    }
    redirect('customer/detail/'.$customer_id);
  }

  public function detail($customer_id){
    $dataContent = array(
      'customer_id' => $customer_id,
      'customer' => $this->Customer_model->get_data($customer_id)
    );
    $content = $this->load->view('customer/detail', $dataContent, true);
    $title = 'ข้อมูลลูกค้า';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }

  public function delete($customer_id){
    $this->Customer_model->delete($customer_id);
    redirect('customer');
  }
}
