<?php
class Car extends CI_Controller{
  var $SEL_NAV = 'car';
  public function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('car/Car_model', 'carbrand/Carbrand_model', 'carmodel/Carmodel_model'));
  }
  public function index(){
    $dataContent = array(
      'search_carcode' => $this->input->get('carcode'),
      'search_brand' => (isset($_GET['carbrand']))?$this->input->get('carbrand'):'all',
      'search_model' => (isset($_GET['carmodel']))?$this->input->get('carmodel'):'all',
      'search_color' => $this->input->get('color')
    );
    $dataContent['car_list'] = $this->Car_model->get_list($dataContent);
    $content = $this->load->view('car/index', $dataContent, true);
    $title = 'ข้อมูลรถ';
    $this->Masterpage_service->add_js('assets/js/page/car/index.js');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form($car_id = 0){
    $car = $this->Car_model->get_data($car_id);
    $carmodel = ($car)?$this->Carmodel_model->get_data($car->model_id):false;
    $dataContent = array(
      'car_id' => $car_id,
      'car' => $car,
      'carmodel' => $carmodel
    );
    $dataContent['car_id'] = $car_id;
    $dataContent['car'] = $this->Car_model->get_data($car_id);
    $content = $this->load->view('car/form', $dataContent, true);
    $title = ($car_id == 0)? 'เพิ่มข้อมูลรถ':'แก้ไขข้อมูลรถ';
    $this->Masterpage_service->add_js('assets/js/page/car/form.js');
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($car_id){
    $data = array(
      'car_registration' => $this->input->post('car_registration'),
      'car_color' => $this->input->post('car_color'),
      'model_id' => $this->input->post('ddl_model')
    );
    if($car_id == 0){
      $car_id = $this->Car_model->insert($data);
    }else{
      $this->Car_model->update($car_id, $data);
    }
    redirect('car/detail/'.$car_id);
  }
  public function detail($car_id){
    $car = $this->Car_model->get_data($car_id);
    $model = $this->Carmodel_model->get_data($car->model_id);
    $dataContent = array(
      'car_id' => $car_id,
      'car' => $this->Car_model->get_data($car_id),
      'model_name' => $model->model_name,
      'brand_name' => $this->Carbrand_model->get_name($model->brand_id)
    );
    $content = $this->load->view('car/detail', $dataContent, true);
    $title = 'ข้อมูลรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function delete($car_id){
    $this->Car_model->delete($car_id);
    redirect('car');
  }
}
