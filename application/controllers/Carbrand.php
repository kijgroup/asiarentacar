<?php
class Carbrand extends CI_Controller{
  var $SEL_NAV = 'carbrand';
  public function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model('carbrand/Carbrand_model');
  }
  public function index(){
    $dataContent = array(
      'brand_list' => $this->Carbrand_model->get_list()
    );
    $content = $this->load->view('carbrand/index', $dataContent, true);
    $title = 'ข้อมูลยี่ห้อรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form($brand_id = 0){
    $dataContent = array(
      'brand_id' => $brand_id,
      'brand' => $this->Carbrand_model->get_data($brand_id)
    );
    $content = $this->load->view('carbrand/form', $dataContent, true);
    $title = ($brand_id == 0)?'เพิ่มยี่ห้อรถ':'แก้ไขข้อมูลยี่ห้อรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($brand_id){
    $brand_name = $this->input->post('carbrand');
    if($brand_id == 0){
      $brand_id = $this->Carbrand_model->insert($brand_name);
    }else{
      $this->Carbrand_model->update($brand_id, $brand_name);
    }
    redirect ('carbrand/detail/'.$brand_id);
  }

  public function detail($brand_id){
    $dataContent = array(
      'brand_id' => $brand_id,
      'brand' => $this->Carbrand_model->get_data($brand_id)
    );
    $content = $this->load->view('carbrand/detail', $dataContent, true);
    $title = 'ข้อมูลยี่ห้อรถ';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }

  public function delete($brand_id){
    $this->Carbrand_model->delete($brand_id);
    redirect('carbrand');
  }

}
