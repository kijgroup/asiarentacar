<?php
class Admin extends CI_Controller{
  var $SEL_NAV = 'admin';
  public function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model('admin/Admin_model');
  }
  public function index(){
    $dataContent = array(
      'admin_list' => $this->Admin_model->get_list()
    );
    $content = $this->load->view('admin/index', $dataContent, true);
    $title = 'ข้อมูลเจ้าหน้าที่';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form($admin_id=0, $err_msg =''){
    $dataContent = array(
      'admin_id' => $admin_id,
      'admin' => $this->Admin_model->get_data($admin_id),
      'err_msg' => $err_msg
    );
    $data = array();
    $content = $this->load->view('admin/form', $dataContent, true);
    $title = ($admin_id == 0)? 'เพิ่มเจ้าหน้าที่':'แก้ไขข้อมูลเจ้าหน้าที่';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }
  public function form_post($admin_id){
    $data = array(
      'username' => $this->input->post('username'),
      'admin_name' => $this->input->post('admin_name'),
      'status' => $this->input->post('sample-radio')
    );
    if($admin_id == 0){
      $data['password'] = $this->input->post('password');
      $data['confirmpassword'] = $this->input->post('confirmpassword');
      if($data['password'] != $data['confirmpassword']){
        redirect('admin/formadmin/'.$admin_id.'/password_not_same');
      }
      $admin_id = $this->Admin_model->insert($data);
    }else{
      $this->Admin_model->update($admin_id, $data);
    }
    redirect('admin/detail/'.$admin_id);
  }

  public function detail($admin_id){
    $dataContent = array(
      'admin_id' => $admin_id,
      'admin' => $this->Admin_model->get_data($admin_id)
    );
    $content = $this->load->view('admin/detail', $dataContent, true);
    $title = 'ข้อมูลเจ้าหน้าที่';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }

  public function changepassword($admin_id, $err_msg = ''){
    $dataContent = array(
      'admin_id' => $admin_id,
      'admin' => $this->Admin_model->get_data($admin_id),
      'err_msg' => $err_msg
    );
    $content = $this->load->view('admin/changepassword', $dataContent, true);
    $title = 'เปลี่ยนรหัสผ่านเจ้าหน้าที่';
    $this->Masterpage_service->display($content, $title, $this->SEL_NAV);
  }

  public function changepassword_post($admin_id){
    $data = array(
      'newpassword' => $this->input->post('newpassword'),
      'confirmpassword' => $this->input->post('confirmpassword')
    );
    if($data['newpassword'] == $data['confirmpassword']){
      $this->Admin_model->changepassword($admin_id, $data);
      redirect('admin/detail/'.$admin_id);
    }else{
      redirect('admin/changepassword/'.$admin_id.'/password_not_same');
    }
  }
  public function delete($admin_id){
    $this->Admin_model->delete($admin_id);
    redirect('admin');
  }
}
