<?php
class Login extends CI_Controller{
  public function index($errMsg=''){
    $this->migrate_database();
    $this->Login_service->must_not_login();
    $this->load->view('login/index', array('errMsg'=>$errMsg));
  }
  public function login_post(){
    $this->load->model('admin/Admin_model');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $admin = $this->Admin_model->check_login($username, $password);
    if(!$admin){
      redirect('login/index/missmatch');
    }
    if($admin->status == 'disable'){
      redirect('login/index/disable');
    }
    $this->Login_service->create_session($admin);
    redirect('rent');
  }

  public function logout(){
    $this->Login_service->logout();
    redirect('login');
  }

  private function migrate_database(){
    ini_set('max_execution_time', 300);
    $this->load->library('migration');
    if ($this->migration->current() === FALSE){
      show_error($this->migration->error_string());
    }
  }
}
