<?php
class Pdf_service extends CI_Model {
  public function create_pdf_file($html, $stylesheet, $header, $footer, $folder, $filename){
    $mpdf = $this->setup_pdf($html, $stylesheet, $header, $footer);
    $full_path = $folder.$filename;
    $mpdf->Output($full_path,'F');
    return $full_path;
  }
  public function view_pdf_file($html, $stylesheet, $header, $footer, $filename){
    $mpdf = $this->setup_pdf($html, $stylesheet, $header, $footer);
    $mpdf->Output($filename,'I');
  }
  private function setup_pdf($html, $stylesheet, $header, $footer){
    include(FCPATH."mpdf/mpdf.php");
    $mpdf=new mPDF('th','A4','','THSarabunNew',10,10,0,0,9,9);
    $mpdf->SetHeader($header);
    $mpdf->SetFooter($footer);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0;
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    return $mpdf;
  }
}
