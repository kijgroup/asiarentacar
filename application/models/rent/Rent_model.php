<?php
class Rent_model extends CI_model{
  public function get_list(){
    $this->db->where('is_delete', 'active');
    $this->db->order_by('rent_id','DESC');
    return $this->db->get('tbl_rent');
  }
  public function get_data($rent_id){
    $this->db->where('rent_id', $rent_id);
    $query = $this->db->get('tbl_rent');
      if($query->num_rows() == 0){
        return false;
      }
      return $query->row();
  }
  public function get_list_customer($customer_id){
    $this->db->where('customer_id', $customer_id);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('rent_id', 'DESC');
    return $this->db->get('tbl_rent');
  }
  public function get_name($rent_id){
    $rent = $this->get_data($rent_id);
     if(!$rent){
       return '-';
     }
     return $rent->rent_code;
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_rent');
    return $this->db->insert_id();
  }

  public function update($rent_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('rent_id', $rent_id);
    $this->db->update('tbl_rent');
  }

  public function delete($rent_id){
    $this->db->set('is_delete', 'delete');
    $this->db->where('rent_id', $rent_id);
    $this->db->update('tbl_rent');
  }
  private function set_to_db($data){
    foreach($data as $key => $value){
      $this->db->set($key, $value);
    }
  }
}
