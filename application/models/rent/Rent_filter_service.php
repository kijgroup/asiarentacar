<?php
class Rent_filter_service extends CI_Model{
  public function get_data_content($search_val, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['rent_list'] = $this->get_list($search_val, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val);
    return $data;
  }
  private function get_count($search_val){
    $this->where_transaction($search_val);
    $this->db->select('tbl_rent.rent_id');
    $query = $this->db->get('tbl_rent');
    return $query->num_rows();
  }
  private function get_list($search_val, $page, $per_page){
    $this->where_transaction($search_val);
    $this->db->select('tbl_rent.*');
    $this->db->order_by('tbl_rent.rent_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_rent', $per_page, $offset);
  }
  private function where_transaction($search_val){
    if($search_val != ''){
      $str_where = "(rent_code like '%$search_val%' ";
      $str_where .= "OR rent_company like '%$search_val%' ";
      $str_where .= "OR rent_tax_id like '%$search_val%' ";
      $str_where .= "OR rent_first_name like '%$search_val%' ";
      $str_where .= "OR rent_last_name like '%$search_val%' ";
      $str_where .= "OR rent_age like '%$search_val%' ";
      $str_where .= "OR rent_id_card like '%$search_val%' ";
      $str_where .= "OR rent_issue_card like '%$search_val%' ";
      $str_where .= "OR rent_end_date_card like '%$search_val%' ";
      $str_where .= "OR rent_address like '%$search_val%' ";
      $str_where .= "OR rent_phone like '%$search_val%' ";
      $str_where .= "OR rent_mobile like '%$search_val%' ";
      $str_where .= "OR rent_email like '%$search_val%' ";
      $str_where .= "OR rent_driver_license_id like '%$search_val%' ";
      $str_where .= "OR rent_issue_driver like '%$search_val%' ";
      $str_where .= "OR rent_buddy_first_name like '%$search_val%' ";
      $str_where .= "OR rent_buddy_last_name like '%$search_val%' ";
      $str_where .= "OR rent_buddy_age like '%$search_val%' ";
      $str_where .= "OR rent_buddy_driver_license like '%$search_val%' ";
      $str_where .= "OR rent_buddy_phone like '%$search_val%' ";
      $str_where .= "OR rent_buddy_mobile like '%$search_val%' ";
      $str_where .= "OR rent_note like '%$search_val%' ";
      $str_where .= "OR rent_company_bill like '%$search_val%' ";
      $str_where .= "OR rent_credit_card_id like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    $this->db->where('is_delete', 'active');
  }
}
