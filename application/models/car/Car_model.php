<?php
class Car_model extends CI_model{
  public function get_list($search_data = false){
    if($search_data){
      if($search_data['search_carcode'] != ''){
        $this->db->where('car_registration like', '%'.$search_data['search_carcode'].'%');
      }
      if($search_data['search_color'] != ''){
        $this->db->where('car_color like', '%'.$search_data['search_color'].'%');
      }
      if($search_data['search_brand'] != 'all'){
        $this->db->where('tbl_model.brand_id like', $search_data['search_brand']);
      }
      if($search_data['search_model'] != 'all'){
        $this->db->where('tbl_car.model_id like', $search_data['search_model']);
      }
    }
    $this->db->join('tbl_model', 'tbl_model.model_id = tbl_car.model_id', 'left');
    $this->db->where('tbl_car.is_delete', 'active');
    $this->db->order_by('tbl_car.car_id', 'DESC');
    return $this->db->get('tbl_car');
  }
  public function get_data($car_id){
    $this->db->where('car_id', $car_id);
    $query = $this->db->get('tbl_car');
    if ($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($car_id){
    $car = $this->get_data($car_id);
    if(!$car){
      return '-';
    }
    return $car->car_registration;
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_car');
    return $this->db->insert_id();

  }
  public function update($car_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('car_id', $car_id);
    $this->db->update('tbl_car');
  }
  public function delete($car_id){
    $this->db->set('is_delete', 'delete');
    $this->db->where('car_id', $car_id);
    $this->db->update('tbl_car');
  }
  private function set_to_db($data){
    $this->db->set('car_registration',$data['car_registration']);
    $this->db->set('car_color', $data['car_color']);
    $this->db->set('model_id', $data['model_id']);
  }
}
