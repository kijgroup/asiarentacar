<?php
class Carmodel_model extends CI_Model{
  public function get_list($brand_id = 'all'){
    if($brand_id != 'all'){
      $this->db->where('brand_id', $brand_id);
    }
    $this->db->order_by('model_id', 'DESC');
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_model');
  }
  public function get_data($model_id){
    $this->db->where('model_id', $model_id);
    $query = $this->db->get('tbl_model');
      if($query->num_rows() == 0){
        return false;
      }
      return $query->row();
  }
  public function get_name($model_id){
    $model = $this->get_data($model_id);
    if(!$model){
      return '-';
    }
    return $model->model_name;
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_model');
    return $this->db->insert_id();
  }
  public function update($model_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('model_id', $model_id);
    $this->db->update('tbl_model');
  }
  public function delete($model_id){
    $this->db->set('is_delete', 'delete');
    $this->db->where('model_id', $model_id);
    $this->db->update('tbl_model');
  }
  private function set_to_db($data){
    $this->db->set('model_name', $data['model_name']);
    $this->db->set('brand_id', $data['brand_id']);
    $this->db->set('car_type', $data['car_type']);
  }
}
