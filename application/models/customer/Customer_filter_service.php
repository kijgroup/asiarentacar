<?php
class Customer_filter_service extends CI_Model{
  public function get_data_content($search_val, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['customer_list'] = $this->get_list($search_val, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val);
    return $data;
  }
  private function get_count($search_val){
    $this->where_transaction($search_val);
    $this->db->select('tbl_customer.customer_id');
    $query = $this->db->get('tbl_customer');
    return $query->num_rows();
  }
  private function get_list($search_val, $page, $per_page){
    $this->where_transaction($search_val);
    $this->db->select('tbl_customer.*');
    $this->db->order_by('tbl_customer.customer_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_customer', $per_page, $offset);
  }
  private function where_transaction($search_val){
    if($search_val != ''){
      $str_where = "(company like '%$search_val%' ";
      $str_where .= "OR tax_id like '%$search_val%' ";
      $str_where .= "OR first_name like '%$search_val%' ";
      $str_where .= "OR last_name like '%$search_val%' ";
      $str_where .= "OR id_card like '%$search_val%' ";
      $str_where .= "OR phone like '%$search_val%' ";
      $str_where .= "OR mobile like '%$search_val%' ";
      $str_where .= "OR email like '%$search_val%' ";
      $str_where .= "OR driver_license_id like '%$search_val%' ";
      $str_where .= "OR category like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    $this->db->where('is_delete', 'active');
  }
}
