<?php
class Customer_search_model extends CI_Model{
  public function search_data($data){
    $this->where_data($data);
    $this->db->where('is_delete', 'active');
    $customer_list = $this->db->get('tbl_customer');
    return array(
      'customer_list' => $customer_list
    );
  }
  private function where_data($data){
    $this->db_like_value('company', $data['search_company']);
    $this->db_like_value('tax_id', $data['search_tax_id']);
    $this->db_like_value('first_name', $data['search_first_name']);
    $this->db_like_value('last_name', $data['search_last_name']);
    $this->db_like_value('phone', $data['search_phone']);
    $this->db_like_value('mobile', $data['search_mobile']);
    $this->db_like_value('email', $data['search_email']);
    $this->db_like_value('driver_license_id', $data['search_driver_license_id']);
    $this->db_like_value('id_card', $data['search_id_card']);
    $this->db_like_value('category', $data['search_category']);
  }
  private function db_like_value($column_name, $value){
    if($value != ''){
      $this->db->like($column_name, $value);
    }
  }
}
