<?php
class Customer_model extends CI_Model{
  public function get_data($customer_id){
    $this->db->where('customer_id', $customer_id);
    $query = $this->db->get('tbl_customer');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_customer');
    $customer_id = $this->db->insert_id();
    return $customer_id;
  }
  public function update($customer_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('customer_id', $customer_id);
    $this->db->update('tbl_customer');
  }
  public function delete($customer_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('customer_id', $customer_id);
    $this->db->update('tbl_customer');
  }
  private function set_to_db($data){
    $this->db->set('company', $data['company']);
    $this->db->set('tax_id', $data['tax_id']);
    $this->db->set('pre_name', $data['pre_name']);
    $this->db->set('first_name', $data['first_name']);
    $this->db->set('last_name', $data['last_name']);
    $this->db->set('age', $data['age']);
    $this->db->set('id_card', $data['id_card']);
    $this->db->set('issue_card', $data['issue_card']);
    $this->db->set('end_date_card', $data['end_date_card']);
    $this->db->set('address', $data['address']);
    $this->db->set('phone', $data['phone']);
    $this->db->set('mobile', $data['mobile']);
    $this->db->set('email', $data['email']);
    $this->db->set('driver_license_id', $data['driver_license_id']);
    $this->db->set('issue_driver', $data['issue_driver']);
    $this->db->set('birthday', $data['birthday']);
    $this->db->set('category', $data['category']);
  }
}
