<?php

Class Login_service extends CI_model{
  public function must_login(){
    if(!$this->session->userdata('is_login')){
      redirect('login');
    }
  }
  public function must_not_login(){
    if($this->session->userdata('is_login')){
      redirect('rent');
    }
  }
  public function create_session($admin){
    $userdata = array(
      'admin_id' => $admin->admin_id,
      'usernme' => $admin->username,
      'admin_name' => $admin->admin_name,
      'is_login' => true
    );
    $this->session->set_userdata($userdata);
  }
  public function logout(){
    $this->session->sess_destroy();
  }
}
