<?php
class Admin_model extends CI_Model{
  VAR $PASSWORD_KEY = 'asiarent';
  public function get_list(){
    $this->db->order_by('admin_id','DESC');
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_admin');
  }
  public function check_login($username, $password){
    $this->db->where('username', $username);
    $this->db->where('password', md5($password.$this->PASSWORD_KEY));
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_admin');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_data($admin_id){
    $this->db->where('admin_id', $admin_id);
    $query = $this->db->get('tbl_admin');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($admin_id){
    $admin = $this->get_data($admin_id);
    if(!$admin){
      return '-';
    }
    return $admin->admin_name;
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->set_password($data['password']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_admin');
    return $this->db->insert_id();
  }
  public function update($admin_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
  }
  public function delete($admin_id){
    $this->db->set('is_delete', 'delete');
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
  }
  public function changepassword($admin_id, $data){
    $this->set_password($data['password']);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
  }
  private function set_to_db($data){
    $this->db->set('username', $data['username']);
    $this->db->set('admin_name', $data['admin_name']);
    $this->db->set('status', $data['status']);
  }
  private function set_password($password){
    $this->db->set('password', md5($password.$this->PASSWORD_KEY));
  }
  public function encrypt_password($password){
    return md5($password.$this->PASSWORD_KEY);
  }
}
