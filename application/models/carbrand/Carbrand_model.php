<?php
class Carbrand_model extends CI_model{
  public function get_list(){
    $this->db->order_by('brand_id','DESC');
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_brand');
  }

  public function get_data($brand_id){
    $this->db->where('brand_id', $brand_id);
    $query = $this->db->get('tbl_brand');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }

  public function get_name($brand_id){
    $brand = $this->get_data($brand_id);
    if(!$brand){
      return '-';
    }
    return $brand->brand_name;
  }

  public function insert($brand_name){
    $this->db->set('brand_name', $brand_name);
    $this->db->set('create_date','NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->set('update_date','NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_brand');
    return $this->db->insert_id();
  }

  public function update($brand_id, $brand_name){
    $this->db->set('brand_name', $brand_name);
    $this->db->set('update_date','NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('brand_id', $brand_id);
    $this->db->update('tbl_brand');
  }

  public function delete($brand_id){
    $this->db->set('is_delete', 'delete');
    $this->db->where('brand_id', $brand_id);
    $this->db->update('tbl_brand');
  }
}
