<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_car_type extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_model', array(
      'car_type' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'default' => '',
        'after' => 'brand_id',
      ),
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_model', 'car_type');
  }
  
}