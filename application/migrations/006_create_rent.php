<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_rent extends CI_Migration {
  public function up(){
    $this->create_rent_table();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_rent');
  }

  private function create_rent_table(){
    $this->dbforge->add_field(array(
      'rent_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'customer_id' => array(
        'type' => 'INT',
        'constraint' => 11,
      ),
      'car_id' => array(
        'type' => 'INT',
        'constraint' => 11,
      ),
      'brand_id' => array(
        'type' => 'INT',
        'constraint' => 11,
      ),
      'model_id' => array(
        'type' => 'INT',
        'constraint' => 11,
      ),
      'rent_date' => array(
        'type' => 'DATE',
      ),
      'rent_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_company' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_tax_id' => array(
        'type' => 'CHAR',
        'constraint' => '13',
      ),
      'rent_first_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '30',
      ),
      'rent_last_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_age' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'rent_id_card' => array(
        'type' => 'CHAR',
        'constraint' => '13',
      ),
      'rent_issue_card' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_end_date_card' => array(
        'type' => 'DATE',
      ),
      'rent_address' => array(
        'type' => 'TEXT',
      ),
      'rent_phone' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_email' => array(
        'type' => 'VARCHAR',
        'constraint' => '30',
      ),
      'rent_driver_license_id' => array(
        'type' => 'CHAR',
        'constraint' => '13',
      ),
      'rent_issue_driver' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_birthday' => array(
        'type' => 'DATE',
      ),
      'rent_buddy_first_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '30',
      ),
      'rent_buddy_last_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_buddy_age' => array(
        'type' => 'CHAR',
        'constraint' => '2',
      ),
      'rent_buddy_driver_license' => array(
        'type' => 'CHAR',
        'constraint' => '13',
      ),
      'rent_buddy_issue_driver' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_buddy_end_date' => array(
        'type' => 'DATE',
      ),
      'rent_buddy_address' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'rent_buddy_phone' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_buddy_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_start_km' => array(
        'type' => 'INT',
        'constraint' => '50',
      ),
      'rent_start_date' => array(
        'type' => 'DATE',
      ),
      'rent_start_time' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_start_location' => array(
        'type' => 'TEXT',
      ),
      'rent_end_km' => array(
        'type' => 'INT',
        'constraint' => '50',
      ),
      'rent_end_date' => array(
        'type' => 'DATE',
      ),
      'rent_end_time' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'rent_end_location' => array(
        'type' => 'TEXT',
      ),
      'rent_totalday' => array(
        'type' => 'VARCHAR',
        'constraint' => '5',
      ),
      'rent_inclusive_rate' => array(
        'type' => 'FLOAT',
      ),
      'rent_total' => array(
        'type' => 'FLOAT',
      ),
      'rent_total_ldw' => array(
        'type' => 'FLOAT',
      ),
      'rent_total_pae' => array(
        'type' => 'FLOAT',
      ),
      'rent_total_delivery_and_pickup' => array(
        'type' => 'FLOAT',
      ),
      'rent_total_other' => array(
        'type' => 'FLOAT',
      ),
      'rent_sub_total' => array(
        'type' => 'FLOAT',
      ),
      'rent_vat' => array(
        'type' => 'FLOAT',
      ),
      'rent_grand_total' => array(
        'type' => 'FLOAT',
      ),
      'rent_oil_price' => array(
        'type' => 'FLOAT',
      ),
      'rent_payment' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'rent_type_credit_card' => array(
        'type' => 'VARCHAR',
        'constraint' => '10',
      ),
      'rent_credit_card_id' => array(
        'type' => 'CHAR',
        'constraint' => '16',
      ),
      'rent_credit_card_end_date' => array(
        'type' => 'DATE',
      ),
      'rent_company_bill' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'rent_bill_address' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'rent_bill_credit_date' => array(
        'type' => 'VARCHAR',
        'constraint' => '5',
      ),
      'rent_payment_other' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'rent_note' => array(
        'type' => 'TEXT',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('rent_id', TRUE);
    $this->dbforge->add_key(array('customer_id', 'is_delete'));
    $this->dbforge->create_table('tbl_rent');
  }

}
