<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_customer extends CI_Migration {
  public function up(){
    $this->create_customer_table();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_customer');
  }

  private function create_customer_table(){
    $this->dbforge->add_field(array(
      'customer_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'company' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'tax_id' => array(
        'type' => 'CHAR',
        'constraint' => '13',
      ),
      'first_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'last_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'age' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'id_card' => array(
        'type' => 'VARCHAR',
        'constraint' => '13',
      ),
      'issue_card' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'end_date_card' => array(
        'type' => 'DATE',
      ),
      'address' => array(
        'type' => 'TEXT',
      ),
      'phone' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'email' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'driver_license_id' => array(
        'type' => 'VARCHAR',
        'constraint' => '13',
      ),
      'issue_driver' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'birthday' => array(
        'type' => 'DATE',
      ),
      'category' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('customer_id', TRUE);
    $this->dbforge->add_key(array('company', 'is_delete'));
    $this->dbforge->create_table('tbl_customer');
  }

}
