<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_admin extends CI_Migration {
  public function up(){
    $this->create_admin_table();
    $this->add_admin();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_admin');
  }

  private function create_admin_table(){
    $this->dbforge->add_field(array(
      'admin_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'username' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'password' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'admin_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'status' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'disable'),
        'default' => 'active'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 255,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 255,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('admin_id', TRUE);
    $this->dbforge->add_key(array('username', 'is_delete'));
    $this->dbforge->create_table('tbl_admin');
  }
  private function add_admin(){
    $this->load->model('admin/Admin_model');
    $password = $this->Admin_model->encrypt_password('asiarent');
    $this->db->set('username', 'admin');
    $this->db->set('password', $password);
    $this->db->set('admin_name', 'Administrator');
    $this->db->set('status', 'active');
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_admin');
  }
}
