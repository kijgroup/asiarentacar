<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_rent_pre_name extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_rent', array(
      'rent_pre_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'default' => '',
        'after' => 'rent_tax_id',
      ),
      'rent_buddy_pre_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'default' => '',
        'after' => 'rent_birthday',
      ),
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_rent', 'rent_pre_name');
    $this->dbforge->drop_column('tbl_rent', 'rent_buddy_pre_name');
  }
  
}