<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_model extends CI_Migration {
  public function up(){
    $this->create_model_table();
    $this->add_model();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_model');
  }

  private function create_model_table(){
    $this->dbforge->add_field(array(
      'model_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'brand_id' => array(
        'type' => 'INT',
        'constraint' => '11',
      ),
      'model_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('model_id', TRUE);
    $this->dbforge->add_key(array('model_name', 'is_delete'));
    $this->dbforge->create_table('tbl_model');
  }
  private function add_model(){
    $this->db->set('model_name', 'Jazz');
    $this->db->insert('tbl_model');
  }
}
