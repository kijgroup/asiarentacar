<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_customer_pre_name extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_customer', array(
      'pre_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'default' => '',
        'after' => 'tax_id',
      ),
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_customer', 'pre_name');
  }
  
}