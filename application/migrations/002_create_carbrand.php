<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_carbrand extends CI_Migration {
  public function up(){
    $this->create_brand_table();
    $this->add_brand();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_brand');
  }

  private function create_brand_table(){
    $this->dbforge->add_field(array(
      'brand_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'brand_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('brand_id', TRUE);
    $this->dbforge->add_key(array('brand_name', 'is_delete'));
    $this->dbforge->create_table('tbl_brand');
  }
  private function add_brand(){
    $this->db->set('brand_name', 'Honda');
    $this->db->insert('tbl_brand');
  }
}
