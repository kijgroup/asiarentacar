<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_car extends CI_Migration {
  public function up(){
    $this->create_car_table();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_car');
  }

  private function create_car_table(){
    $this->dbforge->add_field(array(
      'car_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'model_id' => array(
        'type' => 'INT',
        'constraint' => '11',
      ),
      'car_registration' => array(
        'type' => 'VARCHAR',
        'constraint' => '7',
      ),
      'car_color' => array(
        'type' => 'VARCHAR',
        'constraint' => '10',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'VARCHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('car_id', TRUE);
    $this->dbforge->add_key(array('car_color', 'is_delete'));
    $this->dbforge->create_table('tbl_car');
  }
}
