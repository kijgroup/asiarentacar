$(document).ready(function() {
  $("#searchForm").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    onSearchSubmit(action);
  });
  $("#searchForm").submit();
});
function onSearchSubmit(action) {
  $("#result").empty().append('Loading ...');
  var search_val = $('#search_val').val();
  getContent(action, search_val, 1, 20);
}
function getContent(action, search_val, page, per_page){
  $("#result").empty().append('Loading ...');
  var posting = $.post(action, {
    search_val: search_val,
    page: page,
    per_page: per_page
  });
  posting.done(function(data) {
    $("#result").empty().append(data);
    setOnChangePerPage();
    setOnChangePage();
  });
  posting.fail(function(data){
    setTimeout(function() { getContent(action, search_val, page, per_page); }, 5000);
  });
}
function setOnChangePerPage(){
  $('.ddlPerPage').change(function(){
    var per_page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var sort = $(this).data('sort');
    var order = $(this).data('order');
    getContent(action, search_val, 1, per_page);
  });
}
function setOnChangePage(){
  $('.ddlPager').change(function(){
    var page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var per_page = $(this).data('per_page');
    getContent(action, search_val, page, per_page);
  });
}
