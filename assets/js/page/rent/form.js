var old_brand = '';
var old_model = '';
var old_car = '';
$(document).ready(function(){
  init_search_customer();
  init_input();
  init_car();
  init_price();
  init_prename();
  $('#rent_payment').on('change', filter_payment);
  filter_payment();
  $('.form-validate').validate();
});
function init_search_customer(){
  $('#btn-search-customer').on('click', search_customer);
  $('#btn-new-customer').on('click', new_customer);
}
function search_customer(){
  var action = $('#search-customer-group').data('searchurl');
  $("#search-customer-result").empty().append('Loading ...');
  var posting = $.post(action, {
    search_company: $('#search_company').val(),
    search_tax_id: $('#search_tax_id').val(),
    search_first_name: $('#search_first_name').val(),
    search_last_name: $('#search_last_name').val(),
    search_phone: $('#search_phone').val(),
    search_mobile: $('#search_mobile').val(),
    search_email: $('#search_email').val(),
    search_driver_license_id: $('#search_driver_license_id').val(),
    search_id_card: $('#search_id_card').val()
  });
  posting.done(function(data) {
    $("#search-customer-result").empty().append(data);
    $('.btn-sel-customer').on('click', select_customer);
    $('#main-form').hide();
  });
  posting.fail(function(data){
    setTimeout(function() { search_customer(); }, 5000);
  });
}
function select_customer(){
  $('#customer_id').val($(this).data('customerid'));
  $('#rent_company').val($(this).data('company'));
  $('#rent_tax_id').val($(this).data('taxid'));
  $('#rent_pre_name').val($(this).data('prename'));
  $('#rent_first_name').val($(this).data('firstname'));
  $('#rent_last_name').val($(this).data('lastname'));
  $('#rent_age').val($(this).data('age'));
  $('#rent_phone').val($(this).data('phone'));
  $('#rent_mobile').val($(this).data('mobile'));
  $('#rent_email').val($(this).data('email'));
  $('#rent_address').val($(this).data('address'));
  $('#rent_driver_license_id').val($(this).data('driverlicenseid'));
  $('#rent_issue_driver').val($(this).data('issuedriver'));
  $('#rent_birthday').val($(this).data('birthday'));
  $('#rent_id_card').val($(this).data('idcard'));
  $('#rent_issue_card').val($(this).data('issuecard'));
  $('#rent_end_date_card').val($(this).data('enddatecard'));

  $('#search-customer-group').hide();
  $('#main-form').show();
}
function new_customer(){
  $('#search-customer-group').hide();
  $('#main-form').show();
}

function init_input(){
  $('#rent_start_date, #rent_end_date').on('change', calDateDiff);
  /*
  $('.inp_date').datepicker({
    dateFormat: 'yy-mm-dd',
    beforeShow: function() {
      setTimeout(function(){
        $('.ui-datepicker').css({
          'z-index': 1002
        });
      }, 0);
    }
  });
  var dateFrom, dateTo;
  dateFrom = $("#rent_start_date").datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    numberOfMonths: 3
  })
  .on( "change", function() {
    $("#rent_end_date").datepicker( "option", "minDate", getDate( this ) );
    calDateDiff();
  });
  dateTo = $("#rent_end_date").datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    numberOfMonths: 3
  })
  .on( "change", function() {
    $("#rent_start_date").datepicker( "option", "maxDate", getDate( this ) );
    calDateDiff();
  });
  */
}
function getDate(element) {
  var date;
  try {
    //date = $.datepicker.parseDate('yy-mm-dd',element.value );
  } catch( error ) {
    date = null;
  }
  return date;
}
function convert_be_to_date(strBEDate){
  if(strBEDate == ''){
    return null;
  }
  var partsOfBEDate = strBEDate.split('/');
  var yearCE = partsOfBEDate[2] - 543;
  var strDate = yearCE+"-"+partsOfBEDate[1]+"-"+partsOfBEDate[0];
  return new Date(strDate);
}
function calDateDiff(){
  //var start = $('#rent_start_date').datepicker('getDate');
  //var end   = $('#rent_end_date').datepicker('getDate');
  var start = convert_be_to_date($('#rent_start_date').val());
  var end = convert_be_to_date($('#rent_end_date').val());
  var days   = (end - start)/1000/60/60/24;
  console.log('convert', start, end);
  console.log('days', days);
  days = (days < 1 || days > 1000)?1:days;
  $('#rent_totalday').val(days);
  set_price();
}
function init_price(){
  $('.inp_price').on('change', set_price);
}
function set_price(){
  var inclusive_rate = Number($('#rent_inclusive_rate').val());
  var total_day = Number($('#rent_totalday').val());
  var rent_total = inclusive_rate * total_day;
  $('#rent_total').val(rent_total);
  var ldw = Number($('#rent_total_ldw').val());
  var pae = Number($('#rent_total_pae').val());
  var delivery_and_pickup = Number($('#rent_total_delivery_and_pickup').val());
  var other = Number($('#rent_total_other').val());
  var sub_total = rent_total + ldw + pae + delivery_and_pickup + other;
  var vat = Number($('#rent_vat').val());
  if(sub_total != Number($('#rent_sub_total').val())){
    vat = sub_total * 0.07;
    $('#rent_sub_total').val(sub_total);
    $('#rent_vat').val(vat.toFixed(2));
  }else{
    vat = Number($('#rent_vat').val());
  }
  var grand_total = sub_total + vat;
  $('#rent_grand_total').val(grand_total.toFixed(2));
}
function init_car(){
  old_brand = $('#carbrand').val();
  old_model = $('#carmodel').val();
  old_car = $('#car').val();
  $('#carbrand').on('change', filter_brand);
  $('#carmodel').on('change', filter_model);
  $('#car').on('change', filter_car);
  filter_brand();
  filter_model();
  filter_car();
}
function filter_brand(){
  var car_brand = $('#carbrand').val();
  if(car_brand != ''){
    $('.model-brand').hide();
    $('.model-brand-'+car_brand).show();
    $('.car-brand').hide();
    $('.car-brand-'+car_brand).show();
  }else{
    $('.model-brand').show();
    $('.car-brand').show();
  }
  if(old_brand != car_brand){
    $('#carmodel').val('');
    $('#car').val('');
    old_brand = car_brand;
    old_model = '';
    old_car = '';
    filter_car();
  }
}
function filter_model(){
  var car_model = $('#carmodel').val();
  if(old_model != car_model){
    var brand = $('#carmodel').find(':selected').data('brand');
    $('#car').val('');
    old_model = car_model;
    old_car = '';
    if(brand != ''){
      old_brand = brand;
      $('#carbrand').val(brand);
    }
    filter_brand();
    filter_car();
  }
  if(car_model != ''){
    $('.car-model').hide();
    $('.car-model-'+car_model).show();
  }else{
    $('.car-model').show();
  }
}
function filter_car(){
  var car = $('#car').val();
  var car_selected = $('#car').find(':selected');
  if(car != ''){
    var brand = car_selected.data('brand');
    var model = car_selected.data('model');
    old_brand = brand;
    old_model = model;
    $('#carbrand').val(brand);
    $('#carmodel').val(model);
    filter_brand();
    filter_model();
  }
  $('#car_color').html(car_selected.data('color'));
  if(old_car != car){
    old_car = car;
  }
}
function filter_payment(){
  var optionVal = $('#rent_payment').val();
  $('.payment').hide();
  if(optionVal == 'creditcard'){
    $('#payment_type_creditcard').show();
  }
  if(optionVal == 'bill'){
    $('#payment_type_bill').show();
  }
  if(optionVal == 'credit'){
    $('#payment_type_credit').show();
  }
  if(optionVal == 'other'){
    $('#payment_type_other').show();
  }
}
function init_prename(){
  var available_pre_name = [
    'Mr.','Mrs.','Miss','นาย','นาง', 'นางสาว'
  ];
  $( ".pre_name" ).autocomplete({
    source: available_pre_name
  });
}