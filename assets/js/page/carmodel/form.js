$(document).ready(function(){
  var available_car_type = [
    'E-CAR','C-CAR','F-CAR','MVMR'
  ];
  $( "#car_type" ).autocomplete({
    source: available_car_type
  });
});