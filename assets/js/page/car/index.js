$(document).ready(function(){
  $('#carbrand').on('change', filter_brand);
  filter_brand();
});
function filter_brand(){
  var car_brand = $('#carbrand').val();
  if(car_brand != 'all'){
    $('.model-brand').hide();
    $('.model-brand-'+car_brand).show();
  }else{
    $('.model-brand').show();
  }
}
