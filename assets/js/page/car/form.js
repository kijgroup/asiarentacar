var old_brand = '';
$(document).ready(function(){
  old_brand = $('#ddl_brand').val();
  $('#ddl_brand').on('change', filter_brand);
  filter_brand();
});
function filter_brand(){
  var car_brand = $('#ddl_brand').val();
  $('.model-brand').hide();
  $('.model-brand-'+car_brand).show();
  if(old_brand != car_brand){
    $('#ddl_model').val('');
    old_brand = car_brand;
  }
}
