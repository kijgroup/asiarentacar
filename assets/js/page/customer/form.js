$(document).ready(function(){
  var available_pre_name = [
    'Mr.','Mrs.','Miss','นาย','นาง', 'นางสาว'
  ];
  $( "#pre_name" ).autocomplete({
    source: available_pre_name
  });
});